package com.golubGym.controller;

import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ModelAndView;

import com.golubGym.model.Komentar;
import com.golubGym.model.Korisnik;
import com.golubGym.model.TipTreninga;
import com.golubGym.model.Trening;
import com.golubGym.service.KomentarService;
import com.golubGym.service.KorisnikService;
import com.golubGym.service.TreningService;

@SuppressWarnings("unused")
@Controller
@RequestMapping(value="/komentari")
public class KomentarController implements ServletContextAware{
	
	public static final String KOMENTARI_KEY = "komentari";
	public static final String KORISNICI_KEY = "korisnici";
	public static final String KOMENTAR = "komentar";
	
	@Autowired
	private ServletContext servletContext;
	private  String bURL; 
	
	@Autowired
	KomentarService komentarService;
	
	@Autowired
	private KorisnikService korisnikService;
	
	@Autowired
	private TreningService treningService;
	
	/** pristup ApplicationContext */
	@PostConstruct
	public void init() {	
		bURL = servletContext.getContextPath()+"/";
	}
	
	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	} 
	
	@GetMapping
	public ModelAndView index() {
		List<Komentar> komentari = komentarService.findAll();		
		// podaci sa nazivom template-a
		ModelAndView rezultat = new ModelAndView("komentari"); // naziv template-a
		rezultat.addObject("komentari", komentari); // podatak koji se šalje template-u

		return rezultat; // prosleđivanje zahteva zajedno sa podacima template-u
	}
	
	@GetMapping(value="/create")
	public String create(HttpSession session, HttpServletResponse response) throws IOException {
		// autentikacija, autorizacija
		Korisnik korisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		/*if (korisnik == null || !korisnik.isAdministrator()) {
			response.sendRedirect(bURL + "tipoviTreninga");
			return "tipoviTreninga";
		}*/

		return "dodavanjeKomentara";
	}
	
	@PostMapping(value="/create")
	public void create(@RequestParam Long id, @RequestParam String tekstKomentara, @RequestParam Integer ocena, @RequestParam String datum, @RequestParam Long treningId, @RequestParam String status,@RequestParam String anoniman,
			HttpSession session, HttpServletResponse response) throws IOException {
		System.out.println("poziva se post metoda create");
		Korisnik korisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		/*if (korisnik == null || !korisnik.isAdministrator()) {
			response.sendRedirect(bURL + "tipoviTreninga");
			return;
		}*/
	
		// validacija
		if (tekstKomentara.equals("")) {
			response.sendRedirect(bURL + "komentari/create");
			return;
		}
		/*if (ocena.equals("")) {
			response.sendRedirect(bURL + "komentari/create");
			return;
		}*/
		System.out.println(anoniman);
		Korisnik korisnikk = korisnikService.findOne(korisnik.getKorisnickoIme());
		Trening trening = treningService.findOne(treningId);
		boolean a = anoniman.equals("da")?true:false;
		Komentar komentar = new Komentar(id, tekstKomentara, ocena, datum, korisnikk, trening, status, a);
		komentarService.save(komentar);

		response.sendRedirect(bURL + "komentari");
	}
	
	@GetMapping(value="/add")
	public ModelAndView create() {
		List<Korisnik> korisnici = korisnikService.findAll();
		
		// podaci sa nazivom template-a
		ModelAndView rezultat = new ModelAndView("dodavanjeKomentara"); // naziv template-a
		rezultat.addObject("korisnici", korisnici); // podatak koji se šalje template-u

		return rezultat; // prosleđivanje zahteva zajedno sa podacima template-u
	}

	@GetMapping(value="/Details")
	public ModelAndView details(@RequestParam Long id, HttpSession session) throws IOException {

		Komentar komentar = komentarService.findOne(id);

		ModelAndView rezultat = new ModelAndView("komentar");
		rezultat.addObject("komentar", komentar);

		return rezultat;
	}
	
	@PostMapping(value="/delete")
	public void delete(@RequestParam Long id, HttpServletResponse response) throws IOException {		
		Komentar deleted = komentarService.delete(id);
		response.sendRedirect(bURL+"komentari");
	}
	
	@PostMapping(value="/add")
	public void create(@RequestParam String tekstKomentara, @RequestParam Integer ocena, @RequestParam String datum, @RequestParam String korisnickoIme,@RequestParam Long treningId, @RequestParam String status,@RequestParam Boolean anoniman, HttpServletResponse response) throws IOException {				
	
		Korisnik korisnik = korisnikService.findOne(korisnickoIme);
		Trening trening = treningService.findOne(treningId);
		
		Komentar komentar = new Komentar(tekstKomentara, ocena, datum, korisnik, trening, status, anoniman);
		komentarService.save(komentar);
		response.sendRedirect(bURL+"komentari");
	}
}
