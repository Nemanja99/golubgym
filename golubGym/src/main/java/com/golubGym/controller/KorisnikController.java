package com.golubGym.controller;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ModelAndView;

import com.golubGym.model.Korisnik;
import com.golubGym.service.KorisnikService;

@Controller
@RequestMapping(value = "/korisnici")
public class KorisnikController implements ServletContextAware{

public static final String KORISNIK_KEY = "prijavljenikorisnik";
	
	@Autowired
	private ServletContext servletContext;
	private  String bURL; 
	
	@Autowired
	private KorisnikService korisnikService;
	
	/** inicijalizacija podataka za kontroler */
	@PostConstruct
	public void init() {	
		bURL = servletContext.getContextPath()+"/";
	}
	
	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	} 

	@GetMapping(value = "/login")
	public void getLogin(@RequestParam(required = false) String korisnickoIme, @RequestParam(required = false) String lozinka,
			HttpSession session, HttpServletResponse response) throws IOException {
		postLogin(korisnickoIme, lozinka, session, response);
	}

	@PostMapping(value = "/login")
	@ResponseBody
	public void postLogin(@RequestParam(required = false) String korisnickoIme, @RequestParam(required = false) String lozinka,
			HttpSession session, HttpServletResponse response) throws IOException {
		
		Korisnik korisnik = korisnikService.findOne(korisnickoIme, lozinka);
		String greska = "";
		if (korisnik == null)
			greska = "neispravni kredencijali";

		if (!greska.equals("")) {
			PrintWriter out;
			out = response.getWriter();
			File htmlFile = new File("C:/greska.html");
			Document doc = Jsoup.parse(htmlFile, "UTF-8");

			Element body = doc.select("body").first();

			if (!greska.equals("")) {
				Element divGreska = new Element(Tag.valueOf("div"), "").text(greska);
				body.appendChild(divGreska);
			}
			
			Element loginForm = new Element(Tag.valueOf("form"), "").attr("method", "post").attr("action", "korisnici/login");
			Element table = new Element(Tag.valueOf("table"), "");
			Element caption = new Element(Tag.valueOf("caption"), "").text("Prijava korisnika na sistem");
			Element trKorisnickoIme = new Element(Tag.valueOf("tr"), "");
			Element thKorisnickoIme = new Element(Tag.valueOf("th"), "").text("Korisnicko ime:");
			Element tdKorisnickoIme = new Element(Tag.valueOf("td"), "").appendChild(new Element(Tag.valueOf("input"), "").attr("type", "text").attr("name", "KorisnickoIme"));
			Element trLozinka = new Element(Tag.valueOf("tr"), "");
			Element thLozinka = new Element(Tag.valueOf("th"), "").text("Lozinka:");
			Element tdLozinka = new Element(Tag.valueOf("td"), "").appendChild(new Element(Tag.valueOf("input"), "").attr("type", "text").attr("name", "lozinka"));
			Element trSubmit = new Element(Tag.valueOf("tr"), "");
			Element thSubmit = new Element(Tag.valueOf("th"), "");
			Element tdSubmit = new Element(Tag.valueOf("td"), "").appendChild(new Element(Tag.valueOf("input"), "").attr("type", "submit").attr("value", "Prijavi se"));
			
			trKorisnickoIme.appendChild(thKorisnickoIme);
			trKorisnickoIme.appendChild(tdKorisnickoIme);
			trLozinka.appendChild(thLozinka);
			trLozinka.appendChild(tdLozinka);
			trSubmit.appendChild(thSubmit);
			trSubmit.appendChild(tdSubmit);

			table.appendChild(caption);
			table.appendChild(trKorisnickoIme);
			table.appendChild(trLozinka);
			table.appendChild(trSubmit);
			
			loginForm.appendChild(table);

			body.appendChild(loginForm);
			
			out.write(doc.html());
			return;
		}

		if (session.getAttribute(KORISNIK_KEY) != null)
			greska = "korisnik je već prijavljen na sistem morate se prethodno odjaviti<br/>";

		if (!greska.equals("")) {
			response.setContentType("text/html; charset=UTF-8");
			PrintWriter out;
			out = response.getWriter();

			StringBuilder retVal = new StringBuilder();
			retVal.append("<!DOCTYPE html>\r\n" + "<html>\r\n" + "<head>\r\n" + "	<meta charset=\"UTF-8\">\r\n"
					+ "	<base href=\"/golubGym/\">	\r\n" + "	<title>Prijava korisnika</title>\r\n"
					+ "	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/StiloviForma.css\"/>\r\n"
					+ "	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/StiloviHorizontalniMeni.css\"/>\r\n"
					+ "</head>\r\n" + "<body>\r\n" + "	<ul>\r\n"
					+ "		<li><a href=\"registracija.html\">Registruj se</a></li>\r\n" + "	</ul>\r\n");
			if (!greska.equals(""))
				retVal.append("	<div>" + greska + "</div>\r\n");
			retVal.append("	<a href=\"index.html\">Povratak</a>\r\n" + "	<br/>\r\n" + "</body>\r\n" + "</html>");

			out.write(retVal.toString());
			return;
		}
		System.out.println(korisnik);
		session.setAttribute(KORISNIK_KEY, korisnik);

		response.sendRedirect(bURL + "korisnici");
	}
	
	@GetMapping(value="/Logout")
	public void logout(HttpSession session, HttpServletResponse response) throws IOException {
		
		session.invalidate();
		
		response.sendRedirect(bURL);
	}
	
	@GetMapping(value="/logout")
	@ResponseBody
	public void logout(HttpServletRequest request, HttpServletResponse response) throws IOException {	

		Korisnik korisnik = (Korisnik) request.getSession().getAttribute(KORISNIK_KEY);
		String greska = "";
		if(korisnik==null)
			greska="korisnik nije prijavljen<br/>";
		
		if(!greska.equals("")) {
			response.setContentType("text/html; charset=UTF-8");
			PrintWriter out;	
			out = response.getWriter();
			
			StringBuilder retVal = new StringBuilder();
			retVal.append(
					"<!DOCTYPE html>\r\n" + 
					"<html>\r\n" + 
					"<head>\r\n" +
					"	<meta charset=\"UTF-8\">\r\n" + 
					"	<base href=\"/golubGym/\">	\r\n" + 
					"	<title>Prijava korisnika</title>\r\n" + 
					"	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/StiloviForma.css\"/>\r\n" + 
					"	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/StiloviHorizontalniMeni.css\"/>\r\n" + 
					"</head>\r\n" + 
					"<body>\r\n" + 
					"	<ul>\r\n" + 
					"		<li><a href=\"registracija.html\">Registruj se</a></li>\r\n" + 
					"	</ul>\r\n");
			if(!greska.equals(""))
				retVal.append(
					"	<div>"+greska+"</div>\r\n");
			retVal.append(
					"	<form method=\"post\" action=\"PrijavaOdjava/Login\">\r\n" + 
					"		<table>\r\n" + 
					"			<caption>Prijava korisnika na sistem</caption>\r\n" + 
					"			<tr><th>Korisnicko ime:</th><td><input type=\"text\" value=\"\" name=\"korisnickoIme\" required/></td></tr>\r\n" + 
					"			<tr><th>Lozinka:</th><td><input type=\"password\" value=\"\" name=\"lozinka\" required/></td></tr>\r\n" + 
					"			<tr><th></th><td><input type=\"submit\" value=\"Prijavi se\" /></td>\r\n" + 
					"		</table>\r\n" + 
					"	</form>\r\n" + 
					"	<br/>\r\n" + 
					"	<ul>\r\n" + 
					"		<li><a href=\"korisnici/logout\">Odjavi se</a></li>\r\n" + 
					"	</ul>" +
					"</body>\r\n" + 
					"</html>");
			
			out.write(retVal.toString());
			return;
		}
		
		
		request.getSession().removeAttribute(KORISNIK_KEY);
		request.getSession().invalidate();
		response.sendRedirect(bURL+"korisnici/login");
	}
	
	@PostMapping(value = "/registracija")
	public void registracija(@RequestParam(required = true) String korisnickoIme, @RequestParam(required = true) String lozinka, @RequestParam(required = true) String email,
			@RequestParam(required = true) String ime, @RequestParam(required = true) String prezime, @RequestParam (required = true) String datumRodjenja, @RequestParam (required = true) String adresa, @RequestParam (required = true) String brojTelefona, 
			HttpSession session, HttpServletResponse response) throws IOException {
		Korisnik korisnik = new Korisnik(korisnickoIme, lozinka, email, ime, prezime, datumRodjenja, adresa, brojTelefona);
		korisnikService.save(korisnik);
		
		response.sendRedirect(bURL + "korisnici");
	}
	
	@GetMapping(value="/Details")
	public ModelAndView details(@RequestParam String korisnickoIme, 
			HttpSession session, HttpServletResponse response) throws IOException {

		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);

		if (prijavljeniKorisnik == null || (!prijavljeniKorisnik.isAdministrator() && !prijavljeniKorisnik.getKorisnickoIme().equals(korisnickoIme))) {
			response.sendRedirect(bURL + "korisnici");
			return null;
		}

		Korisnik korisnik = korisnikService.findOne(korisnickoIme);
		if (korisnik == null) {
			response.sendRedirect(bURL + "korisnici");
			return null;
		}

		ModelAndView rezultat = new ModelAndView("korisnik");
		rezultat.addObject("korisnik", korisnik);

		return rezultat;
	}
	
	@GetMapping
	@ResponseBody
	public ModelAndView getKorisnici(HttpSession session, HttpServletResponse response){
		List<Korisnik> korisnici = korisnikService.findAll();
		System.out.println("nesto"+ korisnici.size());
		
		// podaci sa nazivom template-a
		ModelAndView rezultat = new ModelAndView("korisnici"); // naziv template-a
		rezultat.addObject("korisnici", korisnici); // podatak koji se šalje template-u

		return rezultat; // prosleđivanje zahteva zajedno sa podacima template-u
	}
	
	
	
	@PostMapping(value="/Edit")
	public void edit(@RequestParam String korisnickoIme, 
			@RequestParam String lozinka, String email, @RequestParam String ime,  @RequestParam String prezime, @RequestParam String datumRodjenja, @RequestParam String adresa, @RequestParam String brojTelefona, @RequestParam(required=false) String administrator,
			HttpSession session, HttpServletResponse response) throws IOException {

		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);

		if (prijavljeniKorisnik == null || (!prijavljeniKorisnik.isAdministrator() && !prijavljeniKorisnik.getKorisnickoIme().equals(korisnickoIme))) {
			response.sendRedirect(bURL + "korisnici");
			return;
		}

		// validacija
		Korisnik korisnik = korisnikService.findOne(korisnickoIme);
		if (korisnik == null) {
			response.sendRedirect(bURL + "korisnici");
			return;
		}
		if (email.equals("")) {
			response.sendRedirect(bURL + "korisnici/Edit?korisnickoIme=" + korisnickoIme);
			return;
		}
		if (ime.equals("")) {
			response.sendRedirect(bURL + "korisnici/Edit?korisnickoIme=" + korisnickoIme);
			return;
		}
		if (prezime.equals("")) {
			response.sendRedirect(bURL + "korisnici/Edit?korisnickoIme=" + korisnickoIme);
			return;
		}
		if (datumRodjenja.equals("")) {
			response.sendRedirect(bURL + "korisnici/Edit?korisnickoIme=" + korisnickoIme);
			return;
		}
		if (adresa.equals("")) {
			response.sendRedirect(bURL + "korisnici/Edit?korisnickoIme=" + korisnickoIme);
			return;
		}
		if (brojTelefona.equals("")) {
			response.sendRedirect(bURL + "korisnici/Edit?korisnickoIme=" + korisnickoIme);
			return;
		}

		if (!lozinka.equals("")) {
			korisnik.setLozinka(lozinka);
		}
		korisnik.setEmail(email);
		korisnik.setIme(ime);
		korisnik.setPrezime(prezime);
		korisnik.setDatumRodjenja(datumRodjenja);
		korisnik.setAdresa(adresa);
		korisnik.setBrojTelefona(brojTelefona);

		if (prijavljeniKorisnik.isAdministrator() && !prijavljeniKorisnik.equals(korisnik)) {
			korisnik.setAdministrator(administrator != null);
		}
		korisnikService.update(korisnik);

		if (!prijavljeniKorisnik.equals(korisnik)) {
			// TODO odjaviti korisnika
		}

		if (prijavljeniKorisnik.isAdministrator()) {
			response.sendRedirect(bURL + "korisnici");
		} else {
			response.sendRedirect(bURL);
		}
	}
	
//	@GetMapping
//	@ResponseBody
//	public String getKorisnici(HttpSession session, HttpServletResponse response) throws IOException {	
//		StringBuilder retVal = new StringBuilder();
//
//		List<Korisnik> korisnici = korisnikService.findAll();
//		if(korisnici.size() == 0) {
//			// nema korisnika TODO doraditi za domaci neku poruku i redirekt
//		} else {
//			retVal.append(
//					"<!DOCTYPE html>\r\n" + 
//					"<html>\r\n" + 
//					"<head>\r\n" + 
//					"	<meta charset=\"UTF-8\">\r\n" + 
//		    		"	<base href=\""+bURL+"\">" + 
//					"	<title>Clanske karte</title>\r\n" + 
//					"	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/StiloviTabela.css\"/>\r\n" + 
//					"	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/StiloviHorizontalniMeni.css\"/>\r\n"+
//					"</head>\r\n" + 
//					"<body> "+
//					"	<ul>\r\n" + 
//					"		<li><a href=\"knjige\">Knjige</a></li>\r\n" + 
//					"		<li><a href=\"clanskeKarte\">Clanske karte</a></li>\r\n" + 
//					"		<li><a href=\"korisnici\">Korisnici</a></li>\r\n" + 
//					"	</ul>\r\n" + 
//					"		<table>\r\n" + 
//					"			<caption>Korisnici</caption>\r\n" + 
//					"			<tr>\r\n" + 
//					"				<th>Ime</th>\r\n" + 
//					"				<th>Prezime</th>\r\n" + 
//					"				<th>Email</th>\r\n" +
//					"				<th></th>\r\n" +
//					"			</tr>\r\n");
//			
//			for (Korisnik k: korisnici) {
//				retVal.append(
//					"			<tr>\r\n" + 
//					"				<td>"+ k.getIme() +"</td>\r\n" + 
//					"				<td>"+ k.getPrezime() +"</td>\r\n" +
//					"				<td>"+ k.getEmail() +"</td>\r\n" +
//					"				<td>" + 
//									"	<form method=\"post\" action=\"korisnici/obrisi\">\r\n" + 
//									"		<input type=\"hidden\" name=\"id\" value=\""+k.getId()+"\">\r\n" + 
//									"		<input type=\"submit\" value=\"Obrisi\">\r\n" + 
//									"	</form>\r\n" +
//					"				</td>\r\n" +
//					"			</tr>\r\n");
//			}
//			retVal.append("</table>\r\n");
//			
//			retVal.append(
//					"</body>\r\n"+
//					"</html>\r\n");		
//		}
//		
//		return retVal.toString();
//	}
	
	@PostMapping(value="/obrisi")
	public void obrisiKorisnika(@RequestParam String korisnickoIme, HttpServletResponse response) throws IOException {				
		korisnikService.delete(korisnickoIme);

		response.sendRedirect(bURL+"korisnici");
	}

}
