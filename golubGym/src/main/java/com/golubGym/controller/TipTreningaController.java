package com.golubGym.controller;

import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ModelAndView;

import com.golubGym.model.Korisnik;
import com.golubGym.model.TipTreninga;
import com.golubGym.model.Trening;
import com.golubGym.service.KorisnikService;
import com.golubGym.service.TipTreningaService;
import com.golubGym.service.TreningService;


@SuppressWarnings("unused")
@Controller
@RequestMapping(value="/tipoviTreninga")
public class TipTreningaController implements ServletContextAware{

	public static final String TIPOVI_TRENINGA_KEY = "tipovi_treninga";
	public static final String KORISNICI_KEY = "korisnici";
	public static final String TIP_TRENINGA = "tip_treninga";
	
	@Autowired
	private ServletContext servletContext;
	private  String bURL; 
	
	@Autowired
	private TipTreningaService tipTreningaService;
	
	@Autowired
	private KorisnikService korisnikService;
	
	@Autowired
	private TreningService treningService;

	/** pristup ApplicationContext */
	@PostConstruct
	public void init() {	
		bURL = servletContext.getContextPath()+"/";
	}
	
	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	} 
	
	@GetMapping
	public ModelAndView index() {
		List<TipTreninga> tipoviTreninga = tipTreningaService.findAll();		
		// podaci sa nazivom template-a
		ModelAndView rezultat = new ModelAndView("tipoviTreninga"); // naziv template-a
		rezultat.addObject("tipoviTreninga", tipoviTreninga); // podatak koji se šalje template-u

		return rezultat; // prosleđivanje zahteva zajedno sa podacima template-u
	}
	
	// GET: clanskeKarte
//	@GetMapping
//	@ResponseBody
//	public String index() {	
//		List<ClanskaKarta> clanskeKarte = clanskaKartaService.findAll();
//		StringBuilder retVal = new StringBuilder();
//		retVal.append(
//				"<!DOCTYPE html>\r\n" + 
//				"<html>\r\n" + 
//				"<head>\r\n" + 
//				"	<meta charset=\"UTF-8\">\r\n" + 
//	    		"	<base href=\""+bURL+"\">" + 
//				"	<title>Clanske karte</title>\r\n" + 
//				"	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/StiloviTabela.css\"/>\r\n" + 
//				"	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/StiloviHorizontalniMeni.css\"/>\r\n"+
//				"</head>\r\n" + 
//				"<body> "+
//				"	<ul>\r\n" + 
//				"		<li><a href=\"knjige\">Knjige</a></li>\r\n" + 
//				"		<li><a href=\"clanskeKarte\">Clanske karte</a></li>\r\n" + 
//				"		<li><a href=\"korisnici\">Korisnici</a></li>\r\n" + 
//				"	</ul>\r\n" + 
//				"		<table>\r\n" + 
//				"			<caption>Clanske karte</caption>\r\n" + 
//				"			<tr>\r\n" + 
//				"				<th>R. br.</th>\r\n" + 
//				"				<th>Registarski broj</th>\r\n" + 
//				"				<th>Vlasnik</th>\r\n" +
//				"				<th></th>\r\n" +
//				"			</tr>\r\n");
//		
//		for (int i=0; i < clanskeKarte.size(); i++) {
//			ClanskaKarta ck = clanskeKarte.get(i);
//			retVal.append(
//				"			<tr>\r\n" + 
//				"				<td class=\"broj\">"+ (i+1) +"</td>\r\n" + 
//				"				<td>"+ ck.getRegistarskiBroj() +"</td>\r\n" +
//				"				<td>"+ ck.getKorisnik() +"</td>\r\n" +
//				"				<td><a href=\"clanskeKarte/details?registarskiBroj="+ck.getRegistarskiBroj()+"\">Vidi detalje</a></td>\r\n" +
//				"			</tr>\r\n");
//		}
//		retVal.append(
//				"		</table>\r\n");
//		retVal.append(
//				"	<ul>\r\n" + 
//				"		<li><a href=\"clanskeKarte/add\">Dodaj novu clansku kartu</a></li>\r\n" + 
//				"	</ul>\r\n");
//		
//		retVal.append(
//				"</body>\r\n"+
//				"</html>\r\n");		
//		return retVal.toString();
//	}
	
	@GetMapping(value="/create")
	public String create(HttpSession session, HttpServletResponse response) throws IOException {
		// autentikacija, autorizacija
		Korisnik korisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		/*if (korisnik == null || !korisnik.isAdministrator()) {
			response.sendRedirect(bURL + "tipoviTreninga");
			return "tipoviTreninga";
		}*/

		return "dodavanjeTipaTreninga";
	}
	
	@PostMapping(value="/create")
	public void create(@RequestParam Long id, @RequestParam String ime, @RequestParam String opis,
			HttpSession session, HttpServletResponse response) throws IOException {
		
		Korisnik korisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		/*if (korisnik == null || !korisnik.isAdministrator()) {
			response.sendRedirect(bURL + "tipoviTreninga");
			return;
		}*/

		// validacija
		if (ime.equals("")) {
			response.sendRedirect(bURL + "tipoviTreninga/create");
			return;
		}
		if (opis.equals("")) {
			response.sendRedirect(bURL + "tipoviTreninga/create");
			return;
		}

		TipTreninga tipTreninga = new TipTreninga(id, ime, opis);
		tipTreningaService.save(tipTreninga);

		response.sendRedirect(bURL + "tipoviTreninga");
	}
	
	@GetMapping(value="/add")
	public ModelAndView create() {
		List<Korisnik> korisnici = korisnikService.findAll();
		
		// podaci sa nazivom template-a
		ModelAndView rezultat = new ModelAndView("dodavanjeTipaTreninga"); // naziv template-a
		rezultat.addObject("korisnici", korisnici); // podatak koji se šalje template-u

		return rezultat; // prosleđivanje zahteva zajedno sa podacima template-u
	}
	
	@GetMapping(value="/Details")
	public ModelAndView details(@RequestParam Long id, HttpSession session) throws IOException {

		TipTreninga tipTreninga = tipTreningaService.findOne(id);

		ModelAndView rezultat = new ModelAndView("tipTreninga");
		rezultat.addObject("tipTreninga", tipTreninga);

		return rezultat;
	}
	
	@PostMapping(value="/delete")
	public void delete(@RequestParam Long id, HttpServletResponse response) throws IOException {		
		TipTreninga deleted = tipTreningaService.delete(id);
		response.sendRedirect(bURL+"tipoviTreninga");
	}
	
	// GET: clanskeKarte/dodaj
//	@GetMapping(value="/add")
//	@ResponseBody
//	public String create() {
//		List<Korisnik> korisnici = korisnikService.findAll();
//		StringBuilder retVal = new StringBuilder();
//		retVal.append(
//				"<!DOCTYPE html>\r\n" + 
//				"<html>\r\n" + 
//				"<head>\r\n" + 
//				"	<meta charset=\"UTF-8\">\r\n" + 
//	    		"	<base href=\""+bURL+"\">" + 
//				"	<title>Dodaj clansku kartu</title>\r\n" + 
//				"	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/StiloviForma.css\"/>\r\n" + 
//				"	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/StiloviHorizontalniMeni.css\"/>\r\n"+
//				"</head>\r\n" + 
//				"<body>\r\n" + 
//				"	<ul>\r\n" + 
//				"		<li><a href=\"knjige\">Knjige</a></li>\r\n" + 
//				"		<li><a href=\"clanskeKarte\">Clanske karte</a></li>\r\n" + 
//				"		<li><a href=\"korisnici\">Korisnici</a></li>\r\n" + 
//				"	</ul>\r\n" + 
//				"	<form method=\"post\" action=\"clanskeKarte/add\">\r\n" + 
//				"		<table>\r\n" + 
//				"			<caption>Clanska karta</caption>\r\n" + 
//				"			<tr><th>Registarski broj:</th><td><input type=\"text\" value=\"\" name=\"registarskiBroj\"/></td></tr>\r\n" +
//				"			<tr><th>Korisnik:</th>" +
//				"				<td>" +
//				"					<select name=\"idKorisnika\">");
//		for (Korisnik k : korisnici) {
//			retVal.append("<option value=\"" + k.getId() + "\">"
//					+ k.getIme() + " " + k.getPrezime() + '(' + k.getEmail() + ')'
//					+ "</option>");
//		}
//				
//		retVal.append("</select>" + 
//				"</td>" + 
//				"</tr>\r\n" + 
//				"<tr><th></th><td><input type=\"submit\" value=\"Dodaj\" /></td>\r\n" + 
//				"</table>\r\n" + 
//				"</form>\r\n" +
//				"<br/>\r\n" +
//				"</body>\r\n"+
//				"</html>\r\n");		
//		return retVal.toString();
//	}
	
	/** obrada podataka forme za unos novog entiteta, post zahtev */
	// POST: clanskeKarte/add
	@PostMapping(value="/add")
	public void create(@RequestParam String ime, @RequestParam String opis, HttpServletResponse response) throws IOException {				
	
		TipTreninga tipTreninga = new TipTreninga(ime, opis);
		tipTreningaService.save(tipTreninga);
		response.sendRedirect(bURL+"tipoviTreninga");
	}
	
	// POST: clanskeKarte/izdajKnjigu -> izdajKnjige sve sa sesije
	/*@SuppressWarnings("unchecked")
	@PostMapping(value="/izdajKnjige")
	public void izdajKnjigu(@RequestParam String registarskiBroj, HttpSession session, HttpServletResponse response) throws IOException {		
		TipTreninga ck = clanskaKartaService.findOneByRegistarskiBroj(registarskiBroj);
		if (ck == null) {
			//todo
		}
		List<Knjiga> zaIznajmljivanje = (List<Knjiga>) session.getAttribute(KnjigeController.KNJIGE_ZA_IZNAJMLJIVANJE);	

		for (Knjiga k : zaIznajmljivanje) {
			if (ck != null) {
				ck.getIznajmljenjeKnjige().add(k);
				ck = clanskaKartaService.update(ck);
				if (ck != null) {
					k.setIzdata(true);
					knjigaService.update(k);
				}
			}	
		}
		
		session.setAttribute(KnjigeController.KNJIGE_ZA_IZNAJMLJIVANJE, new ArrayList<Knjiga>());

		response.sendRedirect(bURL+"knjige");
	}*/
	
	/*@GetMapping(value="/details")
	@ResponseBody
	public ModelAndView details(@RequestParam String registarskiBroj) {
		TipTreninga ck = tipTreningaService.findOneByRegistarskiBroj(registarskiBroj);
		
		// podaci sa nazivom template-a
		ModelAndView rezultat = new ModelAndView("clanskaKarta"); // naziv template-a
		rezultat.addObject("clanskaKarta", ck); // podatak koji se šalje template-u

		return rezultat; // prosleđivanje zahteva zajedno sa podacima template-u
	}*/
	
	// GET: clanskeKarte/details?id=1
//	@GetMapping(value="/details")
//	@ResponseBody
//	public String details(@RequestParam String registarskiBroj) {	
//		ClanskaKarta ck = clanskaKartaService.findOneByRegistarskiBroj(registarskiBroj);
//
//		StringBuilder retVal = new StringBuilder();
//		retVal.append(
//				"<!DOCTYPE html>\r\n" + 
//				"<html>\r\n" + 
//				"<head>\r\n" + 
//				"	<meta charset=\"UTF-8\">\r\n" + 
//	    		"	<base href=\""+bURL+"\">" + 
//				"	<title>Clanska karta</title>\r\n" + 
//				"	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/StiloviForma.css\"/>\r\n" + 
//				"	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/StiloviHorizontalniMeni.css\"/>\r\n"+
//				"</head>\r\n" + 
//				"<body>\r\n" + 
//				"	<ul>\r\n" + 
//				"		<li><a href=\"knjige\">Knjige</a></li>\r\n" + 
//				"		<li><a href=\"clanskeKarte\">Clanske karte</a></li>\r\n" + 
//				"		<li><a href=\"korisnici\">Korisnici</a></li>\r\n" + 
//				"	</ul>\r\n" + 				 
//			"		<table>\r\n" + 
//			"			<caption>Clanska karta</caption>\r\n" + 
//			"			<tr><th>Registarski broj:</th>" +
//			"				<td>" + ck.getRegistarskiBroj() + "</td>" +
//			"			</tr>\r\n" + 
//		 				"<tr><th>Vlasnik:</th>" + 
//			"				 <td>" + ck.getKorisnik().getIme() + " " + ck.getKorisnik().getPrezime() + "</td>" +
//		 	"			</tr>\r\n" + 
//		 	"		</table><br/><br/>\r\n");
//		
//		
//		retVal.append("<table>\r\n" + 
//			"			<caption>Iznajmljene knjige</caption>\r\n" + 
//			"			<tr>" +
//			"				<th>Naziv</th>\r\n" + 
//			"				<th>Registarski broj primerka</th>\r\n" + 
//			"				<th>Jezik</th>\r\n" +
//			"				<th>Broj stranica</th>\r\n" +
//			"				<th></th>\r\n" +
//			"			</tr>\r\n");
//		
//		for (Knjiga k : ck.getIznajmljenjeKnjige()) {
//			retVal.append("<tr>" +
//						  	"<td>" + k.getNaziv() + "</td>" +
//						  	"<td>" + k.getRegistarskiBrojPrimerka() + "</td>" +
//						  	"<td>" + k.getJezik() + "</td>" +
//						  	"<td>" + k.getBrojStranica() + "</td>");
//		
//			if (k.isIzdata()) {
//				retVal.append("<td>" + 
//						"	<form method=\"post\" action=\"clanskeKarte/vratiKnjigu\">\r\n" + 
//						"		<input type=\"hidden\" name=\"idKnjige\" value=\""+k.getId()+"\">\r\n" + 
//						"		<input type=\"hidden\" name=\"registarskiBroj\" value=\""+ck.getRegistarskiBroj() +"\">\r\n" +
//						"		<input type=\"submit\" value=\"Vrati knjigu\">\r\n" + 
//						"		</table>\r\n" + 
//						"	</form>\r\n"
//						  	+ "</td>");
//			}
//			
//			retVal.append("</tr>");
//						  	
//		}
//		 	
//		retVal.append("</table>\r\n" + 
//				"</body>\r\n"+
//				"</html>\r\n");		
//		return retVal.toString();
//	}
	
	// POST: clanskeKarte/vratiKnjigu
	/*@PostMapping(value="/vratiKnjigu")
	public void vratiKnjigu(@RequestParam Long idKnjige, @RequestParam String registarskiBroj, HttpServletResponse response) throws IOException {		
		ClanskaKarta ck = clanskaKartaService.findOneByRegistarskiBroj(registarskiBroj);

		if (ck != null) {
			Knjiga knjiga = knjigaService.findOne(idKnjige);
			ck.getIznajmljenjeKnjige().remove(knjiga);
			ck = clanskaKartaService.update(ck);
			if (ck != null) {
				knjiga.setIzdata(false);
				knjigaService.update(knjiga);
			}
		}

		response.sendRedirect(bURL+"knjige");
	}*/

}