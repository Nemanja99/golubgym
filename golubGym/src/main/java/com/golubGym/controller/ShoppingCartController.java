package com.golubGym.controller;

import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ModelAndView;

import com.golubGym.model.Komentar;
import com.golubGym.model.Korisnik;
import com.golubGym.model.ShoppingCart;
import com.golubGym.model.TipTreninga;
import com.golubGym.model.Trening;
import com.golubGym.service.KorisnikService;
import com.golubGym.service.ShoppingCartService;
import com.golubGym.service.TipTreningaService;

@SuppressWarnings("unused")
@Controller
@RequestMapping(value="/shoppingCarts")
public class ShoppingCartController implements ServletContextAware{
	
public static final String CHOOSEN_TYPE_OF_TRAINING_FOR_USER_KEY = "choosenTypeOfTrainingForUser";
	
	@Autowired
	private KorisnikService korisnikService;
	
	@Autowired
	ShoppingCartService shoppingCartService;
	
	@Autowired
	private TipTreningaService tipTreningaService;
	
	@Autowired
	private ServletContext servletContext;
	private String bURL; 

	@PostConstruct
	public void init() {	
		bURL = servletContext.getContextPath() + "/";			
	}
	
	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	} 

	/*@GetMapping
	public ModelAndView index(
			
			HttpSession session, HttpServletResponse response) throws IOException {		
		

			ModelAndView rezultat = new ModelAndView("shoppingCarts");
			
			return rezultat;
		
	}*/

	@GetMapping
	public ModelAndView index() {
		List<ShoppingCart> shoppingCart = shoppingCartService.findAll();		
		// podaci sa nazivom template-a
		ModelAndView rezultat = new ModelAndView("shoppingCarts"); // naziv template-a
		rezultat.addObject("shoppingCarts", shoppingCart); // podatak koji se šalje template-u

		return rezultat; // prosleđivanje zahteva zajedno sa podacima template-u
	}
	
	@GetMapping(value="/create")
	public String create(HttpSession session, HttpServletResponse response) throws IOException {
		// autentikacija, autorizacija
		Korisnik korisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		/*if (korisnik == null || !korisnik.isAdministrator()) {
			response.sendRedirect(bURL + "tipoviTreninga");
			return "tipoviTreninga";
		}*/

		return "dodavanjeShoppingCarta";
	}
	
	@PostMapping(value="/create")
	public void create(@RequestParam Long id, @RequestParam String nazivTreninga, @RequestParam String trener, @RequestParam Long tipTreningaId, @RequestParam String datumVreme, @RequestParam String cena,
			HttpSession session, HttpServletResponse response) throws IOException {
		System.out.println("poziva se post metoda create");
		Korisnik korisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		/*if (korisnik == null || !korisnik.isAdministrator()) {
			response.sendRedirect(bURL + "tipoviTreninga");
			return;
		}*/
	
		// validacija
		if (nazivTreninga.equals("")) {
			response.sendRedirect(bURL + "shoppingCarts/create");
			return;
		}
		if (trener.equals("")) {
			response.sendRedirect(bURL + "shoppingCarts/create");
			return;
		}
		if (cena.equals("")) {
			response.sendRedirect(bURL + "shoppingCarts/create");
			return;
		}
		if (datumVreme.equals("")) {
			response.sendRedirect(bURL + "shoppingCarts/create");
			return;
		}
		
		Korisnik korisnikk = korisnikService.findOne(korisnik.getKorisnickoIme());
		TipTreninga tipTreninga = tipTreningaService.findOne(tipTreningaId);
	
		ShoppingCart shoppingCart = new ShoppingCart(id, nazivTreninga, trener, tipTreninga , datumVreme, cena,  korisnikk);
		shoppingCartService.save(shoppingCart);

		response.sendRedirect(bURL + "shoppingCarts");
	}
	
	@GetMapping(value="/add")
	public ModelAndView create() {
		List<Korisnik> korisnici = korisnikService.findAll();
		
		// podaci sa nazivom template-a
		ModelAndView rezultat = new ModelAndView("dodavanjeShoppingCarta"); // naziv template-a
		rezultat.addObject("korisnici", korisnici); // podatak koji se šalje template-u

		return rezultat; // prosleđivanje zahteva zajedno sa podacima template-u
	}
	
	/*@PostMapping(value="/Details")
	@SuppressWarnings("unchecked")
	public ModelAndView details(@RequestParam Long id, 
			HttpSession session, HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		
		TipTreninga tipTreninga = tipTreningaService.findOne(id);

	

		List<TipTreninga> tipTreningaSelected = (List<TipTreninga>) session.getAttribute(ShoppingCartController.CHOOSEN_TYPE_OF_TRAINING_FOR_USER_KEY);
		if (!tipTreningaSelected.contains(tipTreninga)) {
			tipTreningaSelected.add(tipTreninga);

		}
		
		
		
		ModelAndView rezultat = new ModelAndView("shoppingCarts");
		
		
		return rezultat;
	}*/
	
	@GetMapping(value="/Details")
	public ModelAndView details(@RequestParam Long id, HttpSession session) throws IOException {

		ShoppingCart shoppingCart = shoppingCartService.findOne(id);

		ModelAndView rezultat = new ModelAndView("shoppingCart");
		rezultat.addObject("shoppingCart", shoppingCart);

		return rezultat;
	}
	
	@PostMapping(value="/delete")
	public void delete(@RequestParam Long id, HttpServletResponse response) throws IOException {		
		ShoppingCart deleted = shoppingCartService.delete(id);
		response.sendRedirect(bURL+"shoppingCarts");
	}
	
	@PostMapping(value="/add")
	public void create(@RequestParam String nazivTreninga, @RequestParam String trener, @RequestParam Long tipTreningaId, @RequestParam String datumVreme, @RequestParam String cena, @RequestParam String korisnickoIme , HttpServletResponse response) throws IOException {				
	
		Korisnik korisnik = korisnikService.findOne(korisnickoIme);
		TipTreninga tipTreninga = tipTreningaService.findOne(tipTreningaId);
		
		ShoppingCart shoppingCart = new ShoppingCart(nazivTreninga, trener, tipTreninga , datumVreme, cena,  korisnik);
		shoppingCartService.save(shoppingCart);
		response.sendRedirect(bURL+"shoppingCarts");
	}
	
	@PostMapping(value="/Buy")
	@SuppressWarnings("unchecked")
	public ModelAndView buy(
			HttpSession session, HttpServletRequest request, HttpServletResponse response) throws IOException {
		
	
		List<TipTreninga> tipTreningaSelected = (List<TipTreninga>) session.getAttribute(ShoppingCartController.CHOOSEN_TYPE_OF_TRAINING_FOR_USER_KEY);
		 session.setAttribute(ShoppingCartController.CHOOSEN_TYPE_OF_TRAINING_FOR_USER_KEY, tipTreningaSelected);
	
		ModelAndView rezultat = new ModelAndView("shoppingCarts");

		return rezultat;
	}
	
	
	@GetMapping(value="/Buy")
	public String getBuy() throws IOException{
//		
//		TipTreninga tipTreninga = tipTreningaService.findOne(id);
//		
//		List<TipTreninga> tipTreningaSelected = (List<TipTreninga>) session.getAttribute(ShoppingCartController.CHOOSEN_TYPE_OF_TRAINING_FOR_USER_KEY);
//		if(tipTreningaSelected.contains(tipTreninga)) {
//	        tipTreningaSelected.remove(tipTreninga);     
//	      }
//		
//		
//		ModelAndView rezultat = new ModelAndView("shoppingCart");
		return "bought.html";
//		return rezultat;
	}
	
	@PostMapping(value="/Remove")
	@SuppressWarnings("unchecked")
	public ModelAndView remove(@RequestParam Long id, 
			HttpSession session, HttpServletRequest request, HttpServletResponse response) throws IOException {
	

		TipTreninga tipTreninga = tipTreningaService.findOne(id);
		
		List<TipTreninga> tipTreningaSelected = (List<TipTreninga>) session.getAttribute(ShoppingCartController.CHOOSEN_TYPE_OF_TRAINING_FOR_USER_KEY);
		if(tipTreningaSelected.contains(tipTreninga)) {
			tipTreningaSelected.remove(tipTreninga);     
	      }
		
	    			

		ModelAndView rezultat = new ModelAndView("shoppingCarts");
		
		return rezultat;
	}

}
