package com.golubGym.controller;

import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ModelAndView;

import com.golubGym.model.ClanskaKartica;
import com.golubGym.model.Korisnik;
import com.golubGym.service.ClanskaKarticaService;
import com.golubGym.service.KorisnikService;

@SuppressWarnings("unused")
@Controller
@RequestMapping(value="/clanskeKartice")
public class ClanskaKarticaController implements ServletContextAware{

	public static final String CLANSKE_KARTICE_KEY = "clanskeKartice";
	public static final String KORISNICI_KEY = "korisnici";
	public static final String CLANSKA_KARTICA = "clanskaKartica";
	
	@Autowired
	private ServletContext servletContext;
	private  String bURL; 
	
	@Autowired
	ClanskaKarticaService clanskaKarticaService;
	
	@Autowired
	private KorisnikService korisnikService;
	
	
	/** pristup ApplicationContext */
	@PostConstruct
	public void init() {	
		bURL = servletContext.getContextPath()+"/";
	}
	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	} 
	
	@GetMapping
	public ModelAndView index() {
		List<ClanskaKartica> clanskeKartice = clanskaKarticaService.findAll();		
		// podaci sa nazivom template-a
		ModelAndView rezultat = new ModelAndView("clanskeKartice"); // naziv template-a
		rezultat.addObject("clanskeKartice", clanskeKartice); // podatak koji se šalje template-u

		return rezultat; // prosleđivanje zahteva zajedno sa podacima template-u
	}
	
	@GetMapping(value="/create")
	public String create(HttpSession session, HttpServletResponse response) throws IOException {
		// autentikacija, autorizacija
		Korisnik korisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		/*if (korisnik == null || !korisnik.isAdministrator()) {
			response.sendRedirect(bURL + "tipoviTreninga");
			return "tipoviTreninga";
		}*/

		return "dodavanjeClanskeKartice";
	}
	
	@PostMapping(value="/create")
	public void create(@RequestParam Long id, @RequestParam String popust, @RequestParam Integer brojPoena,
			HttpSession session, HttpServletResponse response) throws IOException {
		
		Korisnik korisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		/*if (korisnik == null || !korisnik.isAdministrator()) {
			response.sendRedirect(bURL + "tipoviTreninga");
			return;
		}*/

		// validacija
		if (popust.equals("")) {
			response.sendRedirect(bURL + "clanskeKartice/create");
			return;
		}
	/*	if (brojPoena.equals("")) {
			response.sendRedirect(bURL + "clanskeKartice/create");
			return;
		}*/
		
		ClanskaKartica clanskaKartica = new ClanskaKartica(id, popust, brojPoena);
		clanskaKarticaService.save(clanskaKartica);

		response.sendRedirect(bURL + "clanskeKartice");
	}
	
	@GetMapping(value="/add")
	public ModelAndView create() {
		List<Korisnik> korisnici = korisnikService.findAll();
		
		// podaci sa nazivom template-a
		ModelAndView rezultat = new ModelAndView("dodavanjeClanskeKartice"); // naziv template-a
		rezultat.addObject("korisnici", korisnici); // podatak koji se šalje template-u

		return rezultat; // prosleđivanje zahteva zajedno sa podacima template-u
	}

	@GetMapping(value="/Details")
	public ModelAndView details(@RequestParam Long id, HttpSession session) throws IOException {

		ClanskaKartica clanskaKartica = clanskaKarticaService.findOne(id);

		ModelAndView rezultat = new ModelAndView("clanskaKartica");
		rezultat.addObject("clanskaKartica", clanskaKartica);

		return rezultat;
	}
	
	@PostMapping(value="/edit")
	public void edit(@RequestParam Long id, 
			@RequestParam String popust, @RequestParam Integer brojPoena, 
			HttpSession session, HttpServletResponse response) throws IOException {

		Korisnik korisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if (korisnik == null || !korisnik.isAdministrator()) {
			response.sendRedirect(bURL + "clanskeKartice");
			return;
		}

		// validacija
		ClanskaKartica clanskaKartica = clanskaKarticaService.findOne(id);
		if (clanskaKartica == null) {
			response.sendRedirect(bURL + "clanskeKartice");
			return;
		}	
		if (popust == null || popust.equals("")) {
			response.sendRedirect(bURL + "clanskeKartice/Details?id=" + id);
			return;
		}
		
		if (brojPoena == null) {
			response.sendRedirect(bURL + "clanskeKartice/Details?id=" + id);
			return;
		}

		// izmena
		clanskaKartica.setPopust(popust);
		clanskaKartica.setBrojPoena(brojPoena);
		clanskaKarticaService.update(clanskaKartica);

		response.sendRedirect(bURL + "clanskeKartice");
	}
	
	@PostMapping(value="/delete")
	public void delete(@RequestParam Long id, HttpServletResponse response) throws IOException {		
		ClanskaKartica deleted = clanskaKarticaService.delete(id);
		response.sendRedirect(bURL+"clanskeKartice");
	}
	
	@PostMapping(value="/add")
	public void create(@RequestParam String popust, @RequestParam Integer brojPoena, HttpServletResponse response) throws IOException {				
		
		ClanskaKartica clanskaKartica = new ClanskaKartica(popust, brojPoena);
		clanskaKarticaService.save(clanskaKartica);
		response.sendRedirect(bURL+"clanskeKartice");
	}
}