package com.golubGym.controller;

import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ModelAndView;

import com.golubGym.model.TipTreninga;
import com.golubGym.model.Trening;
import com.golubGym.service.TipTreningaService;
import com.golubGym.service.TreningService;

@Controller
@RequestMapping(value="/treninzi")
public class TreningController implements ServletContextAware{

	public static final String TRENINZI_ZA_IZNAJMLJIVANJE = "treninzi_za_iznajmljivanje";

	@Autowired
	private ServletContext servletContext;
	private  String bURL; 
	
	@Autowired
	private TreningService treningService;
	
	@Autowired
	private TipTreningaService tipTreningaService;
	
	/** inicijalizacija podataka za kontroler */
	@PostConstruct
	public void init() {	
		bURL = servletContext.getContextPath()+"/";
	}
	
	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	} 
	
	@GetMapping
	public ModelAndView index() {
		List<Trening> treninzi = treningService.findAll();		
		// podaci sa nazivom template-a
		ModelAndView rezultat = new ModelAndView("treninzi"); // naziv template-a
		rezultat.addObject("treninzi", treninzi); // podatak koji se šalje template-u

		return rezultat; // prosleđivanje zahteva zajedno sa podacima template-u
	}
	
	/** pribavnjanje HTML stanice za prikaz svih entiteta, get zahtev */
	// GET: knjige
//	@GetMapping
//	@ResponseBody
//	public String index() {	
//		List<Knjiga> knjige = knjigaService.findAll();
//			
//		StringBuilder retVal = new StringBuilder();
//		retVal.append(
//				"<!DOCTYPE html>\r\n" + 
//				"<html>\r\n" + 
//				"<head>\r\n" + 
//				"	<meta charset=\"UTF-8\">\r\n" + 
//	    		"	<base href=\""+bURL+"\">" + 
//				"	<title>Knjige</title>\r\n" + 
//				"	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/StiloviTabela.css\"/>\r\n" + 
//				"	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/StiloviHorizontalniMeni.css\"/>\r\n"+
//				"</head>\r\n" + 
//				"<body> "+
//				"	<ul>\r\n" + 
//				"		<li><a href=\"knjige\">Knjige</a></li>\r\n" + 
//				"		<li><a href=\"clanskeKarte\">Clanske karte</a></li>\r\n" + 
//				"		<li><a href=\"korisnici\">Korisnici</a></li>\r\n" + 
//				"	</ul>\r\n" + 
//				"		<table>\r\n" + 
//				"			<caption>Knjige</caption>\r\n" + 
//				"			<tr>\r\n" + 
//				"				<th>R. br.</th>\r\n" + 
//				"				<th></th>\r\n" + 
//				"				<th>Naziv</th>\r\n" + 
//				"				<th>Registarski broj primerka</th>\r\n" + 
//				"				<th>Jezik</th>\r\n" +
//				"				<th>Broj stranica</th>\r\n" +
//				"				<th></th>\r\n" +
//				"			</tr>\r\n");
//		
//		for (int i=0; i < knjige.size(); i++) {
//			Knjiga knjigaIt = knjige.get(i);
//			if (!knjigaIt.isIzdata()) {
//				retVal.append(
//					"			<tr>\r\n" + 
//					"				<td class=\"broj\">"+ (i+1) +"</td>\r\n" + 
//					"				<td><a href=\"knjige/details?id="+knjigaIt.getId()+"\">" +knjigaIt.getNaziv() +"</a></td>\r\n" +
//					"				<td>"+ knjigaIt.getNaziv() +"</td>\r\n" +
//					"				<td>"+ knjigaIt.getRegistarskiBrojPrimerka() +"</td>\r\n" +
//					"				<td>"+ knjigaIt.getJezik() +"</td>\r\n" +
//					"				<td class=\"broj\">"+ knjigaIt.getBrojStranica() +"</td>\r\n" +
//					"				<td>" + 
//					"					<form method=\"post\" action=\"knjige/delete\">\r\n" + 
//					"						<input type=\"hidden\" name=\"id\" value=\""+knjigaIt.getId()+"\">\r\n" + 
//					"						<input type=\"submit\" value=\"Obriši\"></td>\r\n" + 
//					"					</form>\r\n" +
//					"				</td>" +
//					"			</tr>\r\n");
//			}
//		}
//		retVal.append(
//				"		</table>\r\n");
//		retVal.append(
//				"	<ul>\r\n" + 
//				"		<li><a href=\"knjige/add\">Dodaj knjigu</a></li>\r\n" + 
//				"	</ul>\r\n");
//		
//		retVal.append(
//				"</body>\r\n"+
//				"</html>\r\n");		
//		return retVal.toString();
//	}
	
	@GetMapping(value="/add")
	public String create(HttpSession session, HttpServletResponse response){
		return "dodavanjeTreninga"; // stranica za dodavanje treninga
	}
	
	/** pribavnjanje HTML stanice za unos novog entiteta, get zahtev */
	// GET: knjige/dodaj
//	@GetMapping(value="/add")
//	@ResponseBody
//	public String create() {
//		List<Knjiga> knjige = knjigaService.findAll();
//		
//		StringBuilder retVal = new StringBuilder();
//		retVal.append(
//				"<!DOCTYPE html>\r\n" + 
//				"<html>\r\n" + 
//				"<head>\r\n" + 
//				"	<meta charset=\"UTF-8\">\r\n" + 
//	    		"	<base href=\""+bURL+"\">" + 
//				"	<title>Dodaj knjigu</title>\r\n" + 
//				"	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/StiloviForma.css\"/>\r\n" + 
//				"	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/StiloviHorizontalniMeni.css\"/>\r\n"+
//				"</head>\r\n" + 
//				"<body>\r\n" + 
//				"	<ul>\r\n" + 
//				"		<li><a href=\"knjige\">Knjige</a></li>\r\n" + 
//				"		<li><a href=\"clanskeKarte\">Clanske karte</a></li>\r\n" + 
//				"		<li><a href=\"korisnici\">Korisnici</a></li>\r\n" + 
//				"	</ul>\r\n" + 
//				"	<form method=\"post\" action=\"knjige/add\">\r\n" + 
//				"		<table>\r\n" + 
//				"			<caption>Knjiga</caption>\r\n" + 
//				"			<tr><th>Naziv:</th><td><input type=\"text\" value=\"\" name=\"naziv\"/></td></tr>\r\n" + 
//				"			<tr><th>Registarski broj primerka:</th><td><input type=\"text\" value=\"\" name=\"registarskiBrojPrimerka\"/></td></tr>\r\n" +
//				"			<tr><th>Jezik:</th><td><input type=\"text\" value=\"\" name=\"jezik\"/></td></tr>\r\n" +
//				"			<tr><th>Broj stranica:</th><td><input type=\"number\" min=\"1\" "+
//				 					"name=\"brojStranica\"/></td></tr>\r\n" + 
//				"			<tr><th></th><td><input type=\"submit\" value=\"Dodaj\" /></td>\r\n" + 
//				"		</table>\r\n" + 
//				"	</form>\r\n" +
//				"	<br/>\r\n");
//		retVal.append(
//				"</body>\r\n"+
//				"</html>\r\n");		
//		return retVal.toString();
//	}

	/** obrada podataka forme za unos novog entiteta, post zahtev */
	// POST: knjige/add
	@SuppressWarnings("unused")
	@PostMapping(value="/add")
	public void create(@RequestParam Long id, @RequestParam String naziv, @RequestParam String trener, 
			@RequestParam String kratakOpis, @RequestParam Long tipTreningaId, @RequestParam String cena, @RequestParam String vrstaTreninga, @RequestParam String nivoTreninga, @RequestParam String trajanjeTreninga, HttpServletResponse response) throws IOException {		
		
		TipTreninga tipTreninga = tipTreningaService.findOne(tipTreningaId);
		
		if (tipTreninga == null) {
			
		}
		
		Trening trening = new Trening(id, naziv, trener, kratakOpis, tipTreninga, cena, vrstaTreninga, nivoTreninga, trajanjeTreninga);
		Trening saved = treningService.save(trening);
		response.sendRedirect(bURL+"treninzi");
	}
	
	/** obrada podataka forme za izmenu postojećeg entiteta, post zahtev */
	// POST: knjige/edit
	@SuppressWarnings("unused")
	@PostMapping(value="/edit")
	public void Edit(@ModelAttribute Trening treningEdited , HttpServletResponse response) throws IOException {	
		Trening trening = treningService.findOne(treningEdited.getId());
		if(trening != null) {
			if(treningEdited.getNaziv() != null && !treningEdited.getNaziv().trim().equals(""))
				trening.setNaziv(treningEdited.getNaziv());
			if(treningEdited.getTrener() != null && !treningEdited.getTrener().trim().equals(""))
				trening.setTrener(treningEdited.getTrener());
			if(treningEdited.getKratakOpis() != null && !treningEdited.getKratakOpis().trim().equals(""))
				trening.setKratakOpis(treningEdited.getKratakOpis());
			if(treningEdited.getTipTreninga() != null)
				trening.setTipTreninga(treningEdited.getTipTreninga());
			if(treningEdited.getCena() != null && !treningEdited.getCena().trim().equals(""))
				trening.setCena(treningEdited.getCena());
			if(treningEdited.getVrstaTreninga() != null && !treningEdited.getVrstaTreninga().trim().equals(""))
				trening.setVrstaTreninga(treningEdited.getVrstaTreninga());
			if(treningEdited.getNivoTreninga() != null && !treningEdited.getNivoTreninga().trim().equals(""))
				trening.setNivoTreninga(treningEdited.getNivoTreninga());
			if(treningEdited.getTrajanjeTreninga() != null && !treningEdited.getTrajanjeTreninga().trim().equals(""))
				trening.setTrajanjeTreninga(treningEdited.getTrajanjeTreninga());
		}
		Trening saved = treningService.update(trening);
		response.sendRedirect(bURL+"treninzi");
	}
	
	/** obrada podataka forme za za brisanje postojećeg entiteta, post zahtev */
	// POST: knjige/delete
	@SuppressWarnings("unused")
	@PostMapping(value="/delete")
	public void delete(@RequestParam Long id, HttpServletResponse response) throws IOException {		
		Trening deleted = treningService.delete(id);
		response.sendRedirect(bURL+"treninzi");
	}
	
	@GetMapping(value="/details")
	@ResponseBody
	public ModelAndView details(@RequestParam Long id) {	
		Trening trening = treningService.findOne(id);
		
		// podaci sa nazivom template-a
		ModelAndView rezultat = new ModelAndView("trening"); // naziv template-a
		rezultat.addObject("trening", trening); // podatak koji se šalje template-u

		return rezultat; // prosleđivanje zahteva zajedno sa podacima template-u
	}
	
	/** pribavnjanje HTML stanice za prikaz određenog entiteta , get zahtev */
	// GET: knjige/details?id=1
//	@GetMapping(value="/details")
//	@ResponseBody
//	public String details(@RequestParam Long id) {	
//		Knjiga knjiga = knjigaService.findOne(id);
//		
//		StringBuilder retVal = new StringBuilder();
//		retVal.append(
//				"<!DOCTYPE html>\r\n" + 
//				"<html>\r\n" + 
//				"<head>\r\n" + 
//				"	<meta charset=\"UTF-8\">\r\n" + 
//	    		"	<base href=\""+bURL+"\">" + 
//				"	<title>Knjiga</title>\r\n" + 
//				"	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/StiloviForma.css\"/>\r\n" + 
//				"	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/StiloviHorizontalniMeni.css\"/>\r\n"+
//				"</head>\r\n" + 
//				"<body>\r\n" + 
//				"	<ul>\r\n" + 
//				"		<li><a href=\"Knjige\">Knjige</a></li>\r\n" + 
//				"		<li><a href=\"clanskeKarte\">Clanske karte</a></li>\r\n" + 
//				"		<li><a href=\"korisnici\">Korisnici</a></li>\r\n" + 
//				"	</ul>\r\n" + 				
//				"	<form method=\"post\" action=\"knjige/edit\">\r\n" + 
//				"		<input type=\"hidden\" name=\"id\" value=\""+knjiga.getId()+"\">\r\n" + 
//				"		<table>\r\n" + 
//				"			<caption>Knjiga</caption>\r\n" + 
//				"			<tr><th>Naziv:</th><td><input type=\"text\" "+
//				 				"value=\""+knjiga.getNaziv()+"\" name=\"naziv\"/></td></tr>\r\n" + 
//			 				"<tr><th>Registarski broj primerka:</th><td>" + knjiga.getRegistarskiBrojPrimerka() + "</td></tr>\r\n" + 
// 				"			<tr><th>Jezik:</th><td><input type=\"text\" "+
// 				"				value=\""+knjiga.getJezik()+"\" name=\"jezik\"/></td></tr>\r\n" + 
//				"			<tr><th>Broj stranica:</th><td><input type=\"number\" min=\"1\" "+
//				 				"value=\""+knjiga.getBrojStranica()+"\" name=\"brojStranica\"/></td></tr>\r\n" + 
//				"			<tr><th></th><td><input type=\"submit\" value=\"Izmeni\" /></td>\r\n" + 
//				"		</table>\r\n" + 
//				"	</form>\r\n" + 
//				"	<br/>\r\n" + 
//				"	<form method=\"post\" action=\"knjige/delete\">\r\n" + 
//				"		<input type=\"hidden\" name=\"id\" value=\""+knjiga.getId()+"\">\r\n" + 
//				"		<table>\r\n" + 
//				"			<tr><th></th><td><input type=\"submit\" value=\"Obriši\"></td>\r\n" + 
//				"		</table>\r\n" + 
//				"	</form>\r\n" +
//				"	<br/>\r\n" + 
//				"	<br/>\r\n"); 
//			if (!knjiga.isIzdata()) {
//				retVal.append("	<form method=\"post\" action=\"knjige/zeljene/dodaj\">\r\n" + 
//				"		<input type=\"hidden\" name=\"idKnjige\" value=\""+knjiga.getId()+"\">\r\n" + 
//				"		<table>\r\n" + 
//				"			<caption>Dodaj u zeljene knjige</caption>" +	
//				"			<tr><th></th><td><input type=\"submit\" value=\"Dodaj\"></td>\r\n" + 
//				"		</table>\r\n" + 
//				"	</form>\r\n");
//			}
//		
//		retVal.append(
//				"</body>\r\n"+
//				"</html>\r\n");		
//		return retVal.toString();
//	}
	
	// POST: knjige/zeljene/dodaj
	@SuppressWarnings("unchecked")
	@PostMapping(value="/zeljene/dodaj")
	@ResponseBody
	public void dodajUZeljene(@RequestParam Long idTreninga, HttpSession session, HttpServletResponse response) throws IOException {
		List<Trening> zaIznajmljivanje = (List<Trening>) session.getAttribute(TRENINZI_ZA_IZNAJMLJIVANJE);	
		Trening trening = treningService.findOne(idTreninga);
		zaIznajmljivanje.add(trening);
		
		response.sendRedirect(bURL+"treninzi/zeljene");
	}
	
//	GET: knjige/zeljene
	@SuppressWarnings("unchecked")
	@GetMapping(value="/zeljene")
	@ResponseBody
	public ModelAndView dodajZeljene(HttpSession session){
		List<Trening> zaIznajmljivanje = (List<Trening>) session.getAttribute(TRENINZI_ZA_IZNAJMLJIVANJE);
		
		ModelAndView rezultat = new ModelAndView("zaIznajmljivanje"); // naziv template-a
		rezultat.addObject("treninzi", zaIznajmljivanje); // podatak koji se šalje template-u

		return rezultat;
	}
	
	// GET: knjige/zeljene
//	@SuppressWarnings("unchecked")
//	@GetMapping(value="/zeljene")
//	@ResponseBody
//	public String dodajZeljene(HttpSession session) throws IOException {
//		List<Knjiga> zaIznajmljivanje = (List<Knjiga>) session.getAttribute(KNJIGE_ZA_IZNAJMLJIVANJE);	
//
//		StringBuilder retVal = new StringBuilder();
//		retVal.append(
//				"<!DOCTYPE html>\r\n" + 
//				"<html>\r\n" + 
//				"<head>\r\n" + 
//				"	<meta charset=\"UTF-8\">\r\n" + 
//	    		"	<base href=\""+bURL+"\">" + 
//				"	<title>Knjige</title>\r\n" + 
//				"	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/StiloviTabela.css\"/>\r\n" + 
//				"	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/StiloviHorizontalniMeni.css\"/>\r\n"+
//				"</head>\r\n" + 
//				"<body> "+
//				"	<ul>\r\n" + 
//				"		<li><a href=\"knjige\">Knjige</a></li>\r\n" + 
//				"		<li><a href=\"clanskeKarte\">Clanske karte</a></li>\r\n" + 
//				"		<li><a href=\"korisnici\">Korisnici</a></li>\r\n" + 
//				"	</ul>\r\n" + 
//				"		<table>\r\n" + 
//				"			<caption>Zeljene njige</caption>\r\n" + 
//				"			<tr>\r\n" + 
//				"				<th>Naziv</th>\r\n" + 
//				"				<th>Registarski broj primerka</th>\r\n" + 
//				"				<th>Jezik</th>\r\n" +
//				"				<th>Broj stranica</th>\r\n" +
//				"				<th></th>\r\n" +
//				"			</tr>\r\n");
//		
//		for (int i=0; i < zaIznajmljivanje.size(); i++) {
//			Knjiga knjiga = zaIznajmljivanje.get(i);
//			retVal.append(
//				"			<tr>\r\n" + 
//				"				<td>"+ knjiga.getNaziv() +"</td>\r\n" +
//				"				<td>"+ knjiga.getRegistarskiBrojPrimerka() +"</td>\r\n" +
//				"				<td>"+ knjiga.getJezik() +"</td>\r\n" +
//				"				<td class=\"broj\">"+ knjiga.getBrojStranica() +"</td>\r\n" +
//				"				<td>" + 
//				"					<form method=\"post\" action=\"knjige/zeljene/ukloni\">\r\n" + 
//				"						<input type=\"hidden\" name=\"idKnjige\" value=\""+ knjiga.getId()+"\">\r\n" + 
//				"						<input type=\"submit\" value=\"Ukloni iz zeljenih\"></td>\r\n" + 
//				"					</form>\r\n" +
//				"				</td>" +
//				"			</tr>\r\n");
//		}
//		retVal.append(
//				"		</table>\r\n");
//		
//		retVal.append("	<br/>\r\n" + 
//				"	<form method=\"post\" action=\"clanskeKarte/izdajKnjige\">\r\n" + 
//				"		<table>\r\n" + 
//				"			<caption>Izdaj knjige</caption>" +	
//				"			<tr><th>Registarski broj clanske karte</th><td><input type=\"text\" name=\"registarskiBroj\"></td>\r\n" + 
//				"			<tr><th></th><td><input type=\"submit\" value=\"Izdaj\"></td>\r\n" + 
//				"		</table>\r\n" + 
//				"	</form>\r\n");
//	
//		return retVal.toString();
//	}
	
	// POST: knjige/zeljene/dodaj
	@SuppressWarnings("unchecked")
	@PostMapping(value="/zeljene/ukloni")
	@ResponseBody
	public void ukloniIzZeljenih(@RequestParam Long idTreninga, HttpSession session, HttpServletResponse response) throws IOException {
		List<Trening> zaIznajmljivanje = (List<Trening>) session.getAttribute(TRENINZI_ZA_IZNAJMLJIVANJE);	

		for (Trening trening : zaIznajmljivanje) {
			if (trening.getId().equals(idTreninga)) {
				zaIznajmljivanje.remove(trening);
				break;
			}
		}
		response.sendRedirect(bURL+"treninzi/zeljene");
	}
}