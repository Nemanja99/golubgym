package com.golubGym.listeners;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import com.golubGym.controller.TipTreningaController;
import com.golubGym.controller.TreningController;
import com.golubGym.model.Trening;



public class InitHttpSessionListener implements HttpSessionListener{

	/** kod koji se izvrsava po kreiranju sesije */
	public void sessionCreated(HttpSessionEvent arg0) {
		System.out.println("Inicijalizacija sesisje HttpSessionListener...");
//		
//		//pri kreiranju sesije inicijalizujemo je ili radimo neke dodatne aktivnosti
		List<Trening> zaIznajmljivanje = new ArrayList<Trening>();
		String registarskiBrojTT = "";
		HttpSession session  = arg0.getSession();
		System.out.println("session id korisnika je "+session.getId());
		session.setAttribute(TreningController.TRENINZI_ZA_IZNAJMLJIVANJE, zaIznajmljivanje);
		
		session.setAttribute(TipTreningaController.TIP_TRENINGA, registarskiBrojTT);
//		
		System.out.println("Uspeh HttpSessionListener!");
	}
	
	/** kod koji se izvrsava po brisanju sesije */
	public void sessionDestroyed(HttpSessionEvent arg0) {
		System.out.println("Brisanje sesisje HttpSessionListener...");
		
		System.out.println("Uspeh HttpSessionListener!");
	}
	
}
