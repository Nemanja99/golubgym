package com.golubGym.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.golubGym.dao.ShoppingCartDAO;
import com.golubGym.model.ShoppingCart;
import com.golubGym.service.ShoppingCartService;

@Service
public class DatabaseShoppingCartService implements ShoppingCartService{

	@Autowired
	private ShoppingCartDAO shoppingCartDAO;
	
	
	@Override
	public ShoppingCart findOne(Long id) {
		return shoppingCartDAO.findOne(id);
	}
	
	@Override
	public List<ShoppingCart> findOne( String korisnickoIme, Long tipTreningaId) {
		return shoppingCartDAO.findOne( korisnickoIme, tipTreningaId);
	}
	

	@Override
	public List<ShoppingCart> findAll() {
		return shoppingCartDAO.findAll();
	}
	
	@Override
	public List<ShoppingCart> findAll(String korisnickoIme) {
		return shoppingCartDAO.findAll(korisnickoIme);
	}
	
	@Override
	public List<ShoppingCart> findAll2(String tipTreningaId) {
		return shoppingCartDAO.findAll2(tipTreningaId);
	}
	
	@Override
	public ShoppingCart save(ShoppingCart shoppingCart) {
		shoppingCartDAO.save(shoppingCart);
		return shoppingCart;
	}

	@Override
	public ShoppingCart delete(Long tipTreningaId) {
		ShoppingCart shoppingCart = findOne(tipTreningaId);
		if (shoppingCart != null) {
			shoppingCartDAO.delete(tipTreningaId);
		}
		return shoppingCart;
	}
}
