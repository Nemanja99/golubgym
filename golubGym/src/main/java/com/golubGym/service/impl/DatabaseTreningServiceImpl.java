package com.golubGym.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.golubGym.dao.TreningDAO;
import com.golubGym.model.Trening;
import com.golubGym.service.TreningService;


@Service
public class DatabaseTreningServiceImpl implements TreningService{

	@Autowired
	private TreningDAO treningDAO;

	@Override
	public Trening findOne(Long id) {
		return treningDAO.findOne(id);
	}
	
	@Override
	public Trening findOne(String naziv) {
		return treningDAO.findOne(naziv);
	}

	@Override
	public List<Trening> findAll() {
		return treningDAO.findAll();
	}

	@Override
	public Trening save(Trening trening) {
		treningDAO.save(trening);
		return trening;
	}

	@Override
	public Trening update(Trening trening) {
		treningDAO.update(trening);
		return trening;
	}

	@Override
	public Trening delete(Long id) {
		Trening trening = treningDAO.findOne(id);
		if(trening != null) {
			treningDAO.delete(id);
		}
		return trening;
	}
}
