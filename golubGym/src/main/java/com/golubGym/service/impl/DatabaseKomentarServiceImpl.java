package com.golubGym.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.golubGym.dao.KomentarDAO;
import com.golubGym.model.Komentar;
import com.golubGym.service.KomentarService;


@Service
public class DatabaseKomentarServiceImpl implements KomentarService{

	@Autowired
	private KomentarDAO KomentarDAO;

	@Override
	public Komentar findOne(Long id) {
		return KomentarDAO.findOne(id);
	}

	@Override
	public List<Komentar> findAll() {
		return KomentarDAO.findAll();
	}

	@Override
	public Komentar save(Komentar komentar) {
		KomentarDAO.save(komentar);
		return komentar;
	}

	@Override
	public Komentar update(Komentar komentar) {
		KomentarDAO.update(komentar);
		return komentar;
	}

	@Override
	public Komentar delete(Long id) {
		Komentar komentar = KomentarDAO.findOne(id);
		if(komentar != null) {
			KomentarDAO.delete(id);
		}
		return komentar;
	}
}