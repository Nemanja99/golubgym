package com.golubGym.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.golubGym.dao.KorisnikDAO;
import com.golubGym.model.Korisnik;
import com.golubGym.service.KorisnikService;

@Service
public class DatabaseKorisnikServiceImpl implements KorisnikService{
	@Autowired
	private KorisnikDAO korisnikDAO;
	
	/*@Override
	public Korisnik findOneById(Long id) {
		return korisnikDAO.findOne(id);
	}*/

	@Override
	public Korisnik findOne(String korisnickoIme) {
		return korisnikDAO.findOne(korisnickoIme);
	}
	
	@Override
	public Korisnik findOne(Boolean administrator) {
		return korisnikDAO.findOne(administrator);
	}

	@Override
	public Korisnik findOne(String korisnickoIme, String lozinka) {
		return korisnikDAO.findOne(korisnickoIme, lozinka);
	}

	@Override
	public List<Korisnik> findAll() {
		return korisnikDAO.findAll();
	}

	@Override
	public Korisnik save(Korisnik korisnik) {
		korisnikDAO.save(korisnik);
		return korisnik;
	}

	@Override
	public Korisnik update(Korisnik korisnik) {
		korisnikDAO.update(korisnik);
		return korisnik;
	}

	@Override
	public Korisnik delete(String korisnickoIme) {
		Korisnik korisnik = korisnikDAO.findOne(korisnickoIme);
		korisnikDAO.delete(korisnickoIme);
		return korisnik;
	}
}
