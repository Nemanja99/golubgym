package com.golubGym.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.golubGym.dao.TipTreningaDAO;
import com.golubGym.model.TipTreninga;
import com.golubGym.service.TipTreningaService;

@Service
public class DatabaseTipTreningaServiceImpl implements TipTreningaService{

	@Autowired
	private TipTreningaDAO TiptreningaDAO;

	@Override
	public TipTreninga findOne(Long id) {
		return TiptreningaDAO.findOne(id);
	}
	
	@Override
	public TipTreninga findOne(String ime) {
		return TiptreningaDAO.findOne(ime);
	}
	
	
	@Override
	public TipTreninga findOne(String ime, String opis) {
		return TiptreningaDAO.findOne(ime, opis);
	}

	@Override
	public List<TipTreninga> findAll() {
		return TiptreningaDAO.findAll();
	}

	@Override
	public TipTreninga save(TipTreninga trening) {
		TiptreningaDAO.save(trening);
		return trening;
	}

	@Override
	public TipTreninga update(TipTreninga trening) {
		TiptreningaDAO.update(trening);
		return trening;
	}

	@Override
	public TipTreninga delete(Long id) {
		TipTreninga trening = TiptreningaDAO.findOne(id);
		if(trening != null) {
			TiptreningaDAO.delete(id);
		}
		return trening;
	}
}
