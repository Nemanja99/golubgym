package com.golubGym.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.golubGym.dao.ClanskaKarticaDAO;
import com.golubGym.model.ClanskaKartica;
import com.golubGym.service.ClanskaKarticaService;

@Service
public class DatabaseClanskaKarticaServiceImpl implements ClanskaKarticaService{

	@Autowired
	private ClanskaKarticaDAO ClanskaKarticaDAO;

	@Override
	public ClanskaKartica findOne(Long id) {
		return ClanskaKarticaDAO.findOne(id);
	}

	@Override
	public List<ClanskaKartica> findAll() {
		return ClanskaKarticaDAO.findAll();
	}

	@Override
	public ClanskaKartica save(ClanskaKartica clanskaKartica) {
		ClanskaKarticaDAO.save(clanskaKartica);
		return clanskaKartica;
	}

	@Override
	public ClanskaKartica update(ClanskaKartica clanskaKartica) {
		ClanskaKarticaDAO.update(clanskaKartica);
		return clanskaKartica;
	}

	@Override
	public ClanskaKartica delete(Long id) {
		ClanskaKartica clanskaKartica = ClanskaKarticaDAO.findOne(id);
		if(clanskaKartica != null) {
			ClanskaKarticaDAO.delete(id);
		}
		return clanskaKartica;
	}
}