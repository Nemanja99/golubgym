package com.golubGym.service;

import java.util.List;

import com.golubGym.model.Korisnik;

public interface KorisnikService {
	//Korisnik findOneById(Long id);
	Korisnik findOne(String korisnickoIme); 
	Korisnik findOne (Boolean administrator);
	Korisnik findOne(String korisnickoIme, String lozinka);
	List<Korisnik> findAll(); 
	Korisnik save(Korisnik korisnik); 
	Korisnik update(Korisnik korisnik); 
	Korisnik delete(String korisnickoIme); 
}
