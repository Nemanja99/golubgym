package com.golubGym.service;

import java.util.List;

import com.golubGym.model.ClanskaKartica;

public interface ClanskaKarticaService {

	ClanskaKartica findOne(Long id); 
	
	List<ClanskaKartica> findAll(); 
	ClanskaKartica save(ClanskaKartica clanskaKartica); 
	ClanskaKartica update(ClanskaKartica clanskaKartica); 
	ClanskaKartica delete(Long id); 
	
}
