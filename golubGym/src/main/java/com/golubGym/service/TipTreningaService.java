package com.golubGym.service;

import java.util.List;

import com.golubGym.model.TipTreninga;

public interface TipTreningaService {

	TipTreninga findOne(Long id); 
	TipTreninga findOne(String ime); 
	TipTreninga findOne(String ime, String opis);
	List<TipTreninga> findAll(); 
	TipTreninga save(TipTreninga tipTreninga); 
	TipTreninga update(TipTreninga tipTreninga); 
	TipTreninga delete(Long id); 
}
