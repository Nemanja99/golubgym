package com.golubGym.service;

import java.util.List;

import com.golubGym.model.Komentar;

public interface KomentarService {

	Komentar findOne(Long id); 
	
	List<Komentar> findAll(); 
	Komentar save(Komentar komentar); 
	Komentar update(Komentar komentar); 
	Komentar delete(Long id); 
	
}
