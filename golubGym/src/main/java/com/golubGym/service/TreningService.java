package com.golubGym.service;

import java.util.List;

import com.golubGym.model.Trening;



public interface TreningService {

	Trening findOne(Long id); 
	Trening findOne(String naziv);
	List<Trening> findAll(); 
	Trening save(Trening trening); 
	Trening update(Trening trening); 
	Trening delete(Long id); 
}
