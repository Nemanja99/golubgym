package com.golubGym.service;

import java.util.List;

import com.golubGym.model.ShoppingCart;

public interface ShoppingCartService {

	ShoppingCart findOne(Long id);
	
	List<ShoppingCart> findOne( String korisnickoIme, Long tipTreningaId);
	
	List<ShoppingCart> findAll();
	
	List<ShoppingCart> findAll(String korisnickoIme);
	
	List<ShoppingCart> findAll2(String tipTreningaId);
	
	ShoppingCart save(ShoppingCart shoppingCart);
	
	ShoppingCart delete(Long id);
	
}
