package com.golubGym.model;

public class Komentar {
	
	private Long id;
	private String tekstKomentara;
	private int ocena;
	private String datum;
	private Korisnik korisnik;
	private Trening trening;
	private String status;
	private boolean anoniman;
	
	public Komentar(Long id, String tekstKomentara, int ocena, String datum, Korisnik korisnik, Trening trening,
			String status, boolean anoniman) {
		super();
		this.id = id;
		this.tekstKomentara = tekstKomentara;
		this.ocena = ocena;
		this.datum = datum;
		this.korisnik = korisnik;
		this.trening = trening;
		this.status = status;
		this.anoniman = anoniman;
	}
	
	public Komentar(String tekstKomentara, int ocena, String datum, Korisnik korisnik, Trening trening,
			String status, boolean anoniman) {
		super();
		this.tekstKomentara = tekstKomentara;
		this.ocena = ocena;
		this.datum = datum;
		this.korisnik = korisnik;
		this.trening = trening;
		this.status = status;
		this.anoniman = anoniman;
	}



	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTekstKomentara() {
		return tekstKomentara;
	}
	public void setTekstKomentara(String tekstKomentara) {
		this.tekstKomentara = tekstKomentara;
	}
	public int getOcena() {
		return ocena;
	}
	public void setOcena(int ocena) {
		this.ocena = ocena;
	}
	public String getDatum() {
		return datum;
	}
	public void setDatum(String datum) {
		this.datum = datum;
	}
	public Korisnik getKorisnik() {
		return korisnik;
	}
	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}
	public Trening getTrening() {
		return trening;
	}
	public void setTrening(Trening trening) {
		this.trening = trening;
	}
	public boolean isAnoniman() {
		return anoniman;
	}
	public void setAnoniman(boolean anoniman) {
		this.anoniman = anoniman;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
}
