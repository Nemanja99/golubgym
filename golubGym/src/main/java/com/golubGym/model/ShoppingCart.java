package com.golubGym.model;

public class ShoppingCart {
	
	private Long id;
	private String nazivTreninga;
	private String trener;
	private TipTreninga tipTreninga;
	private String datumVreme;
	private String cena;
	private Korisnik korisnik;
	
	public ShoppingCart(Long id, String nazivTreninga, String trener, TipTreninga tipTreninga, String datumVreme,
			String cena, Korisnik korisnik) {
		super();
		this.id = id;
		this.nazivTreninga = nazivTreninga;
		this.trener = trener;
		this.tipTreninga = tipTreninga;
		this.datumVreme = datumVreme;
		this.cena = cena;
		this.korisnik = korisnik;
	}

	public ShoppingCart(String nazivTreninga, String trener, TipTreninga tipTreninga, String datumVreme, String cena, Korisnik korisnik) {
		super();
		this.nazivTreninga = nazivTreninga;
		this.trener = trener;
		this.tipTreninga = tipTreninga;
		this.datumVreme = datumVreme;
		this.cena = cena;
		this.korisnik = korisnik;
	}

	public Korisnik getKorisnik() {
		return korisnik;
	}

	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNazivTreninga() {
		return nazivTreninga;
	}

	public void setNazivTreninga(String nazivTreninga) {
		this.nazivTreninga = nazivTreninga;
	}

	public String getTrener() {
		return trener;
	}

	public void setTrener(String trener) {
		this.trener = trener;
	}

	public TipTreninga getTipTreninga() {
		return tipTreninga;
	}

	public void setTipTreninga(TipTreninga tipTreninga) {
		this.tipTreninga = tipTreninga;
	}

	public String getDatumVreme() {
		return datumVreme;
	}

	public void setDatumVreme(String datumVreme) {
		this.datumVreme = datumVreme;
	}

	public String getCena() {
		return cena;
	}

	public void setCena(String cena) {
		this.cena = cena;
	}

}
