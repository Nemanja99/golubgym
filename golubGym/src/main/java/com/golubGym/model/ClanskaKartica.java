package com.golubGym.model;

public class ClanskaKartica {
	
	private Long id;
	private String popust;
	private int brojPoena;
	
	public ClanskaKartica(Long id, String popust, int brojPoena) {
		super();
		this.id = id;
		this.popust = popust;
		this.brojPoena = brojPoena;
	}
	
	public ClanskaKartica(String popust, int brojPoena) {
		super();
		this.popust = popust;
		this.brojPoena = brojPoena;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getPopust() {
		return popust;
	}
	public void setPopust(String popust) {
		this.popust = popust;
	}
	public int getBrojPoena() {
		return brojPoena;
	}
	public void setBrojPoena(int brojPoena) {
		this.brojPoena = brojPoena;
	}

}
