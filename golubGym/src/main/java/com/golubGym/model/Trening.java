package com.golubGym.model;

public class Trening {

	private Long id;
	private String naziv;
	private String trener;
	private String kratakOpis;
	private String slika;
	private TipTreninga tipTreninga;
	private String cena;
	private String vrstaTreninga;
	private String nivoTreninga;
	private String trajanjeTreninga;
	private String prosecnaOcena;
	
	public Trening(Long id, String naziv, String trener, String kratakOpis,/* String slika,*/ TipTreninga tipTreninga,
			String cena, String vrstaTreninga, String nivoTreninga, String trajanjeTreninga) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.trener = trener;
		this.kratakOpis = kratakOpis;
		//this.slika = slika;
		this.tipTreninga = tipTreninga;
		this.cena = cena;
		this.vrstaTreninga = vrstaTreninga;
		this.nivoTreninga = nivoTreninga;
		this.trajanjeTreninga = trajanjeTreninga;
	}
	public Trening(String naziv, String trener, String kratakOpis,/* String slika,*/ TipTreninga tipTreninga, String cena,
			String vrstaTreninga, String nivoTreninga, String trajanjeTreninga) {
		super();
		this.naziv = naziv;
		this.trener = trener;
		this.kratakOpis = kratakOpis;
		//this.slika = slika;
		this.tipTreninga = tipTreninga;
		this.cena = cena;
		this.vrstaTreninga = vrstaTreninga;
		this.nivoTreninga = nivoTreninga;
		this.trajanjeTreninga = trajanjeTreninga;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public String getTrener() {
		return trener;
	}
	public void setTrener(String trener) {
		this.trener = trener;
	}
	public String getKratakOpis() {
		return kratakOpis;
	}
	public void setKratakOpis(String kratakOpis) {
		this.kratakOpis = kratakOpis;
	}
	public String getSlika() {
		return slika;
	}
	public void setSlika(String slika) {
		this.slika = slika;
	}
	public TipTreninga getTipTreninga() {
		return tipTreninga;
	}
	public void setTipTreninga(TipTreninga tipTreninga) {
		this.tipTreninga = tipTreninga;
	}
	public String getCena() {
		return cena;
	}
	public void setCena(String cena) {
		this.cena = cena;
	}
	public String getVrstaTreninga() {
		return vrstaTreninga;
	}
	public void setVrstaTreninga(String vrstaTreninga) {
		this.vrstaTreninga = vrstaTreninga;
	}
	public String getNivoTreninga() {
		return nivoTreninga;
	}
	public void setNivoTreninga(String nivoTreninga) {
		this.nivoTreninga = nivoTreninga;
	}
	public String getTrajanjeTreninga() {
		return trajanjeTreninga;
	}
	public void setTrajanjeTreninga(String trajanjeTreninga) {
		this.trajanjeTreninga = trajanjeTreninga;
	}
	
	public String getProsecnaOcena() {
		return prosecnaOcena;
	}
	public void setProsecnaOcena(String prosecnaOcena) {
		this.prosecnaOcena = prosecnaOcena;
	}
	@Override
	public String toString() {
		return "Trening [id=" + id + ", naziv=" + naziv + ", trener=" + trener + ", kratakOpis=" + kratakOpis
				+ ", slika=" + slika + ", tipTreninga=" + tipTreninga + ", cena=" + cena + ", vrstaTreninga="
				+ vrstaTreninga + ", nivoTreninga=" + nivoTreninga + ", trajanjeTreninga=" + trajanjeTreninga + "]";
	}
	
}
