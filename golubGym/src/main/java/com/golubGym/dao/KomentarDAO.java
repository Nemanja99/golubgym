package com.golubGym.dao;

import java.util.List;

import com.golubGym.model.Komentar;



public interface KomentarDAO {
	
	public Komentar findOne(Long id);

	public List<Komentar> findAll();

	public int save(Komentar komentar);

	public int update(Komentar komentar);

	public int delete(Long id);
	
}
