package com.golubGym.dao;

import java.util.List;

import com.golubGym.model.Trening;

public interface TreningDAO {

	public Trening findOne(Long id);
	
	public Trening findOne(String naziv);

	public List<Trening> findAll();

	public int save(Trening trening);

	public int update(Trening trening);

	public int delete(Long id);
	
}
