package com.golubGym.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.golubGym.dao.KomentarDAO;
import com.golubGym.dao.KorisnikDAO;
import com.golubGym.dao.TreningDAO;
import com.golubGym.model.Komentar;
import com.golubGym.model.Korisnik;
import com.golubGym.model.Trening;

@Repository
public class KomentarDAOImpl implements KomentarDAO{

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private TreningDAO treningDAO;
	
	@Autowired
	private KorisnikDAO korisnikDAO;
	
	private class KomentarRowCallBackHandler implements RowCallbackHandler {

		private Map<Long, Komentar> komentari = new LinkedHashMap<>();
		
		@Override
		public void processRow(ResultSet resultSet) throws SQLException {
			int index = 1;
			
			Long id = resultSet.getLong(index++);
			String tekstKomentara = resultSet.getString(index++);
			Integer ocena = resultSet.getInt(index++);
			String datum = resultSet.getString(index++);
			String korisnickoIme = resultSet.getString(index++);
			Long treningId = resultSet.getLong(index++);
			String status = resultSet.getString(index++);
			Boolean anoniman =resultSet.getBoolean(index++);
			System.out.println(treningId + "trening");
			Korisnik korisnik = korisnikDAO.findOne(korisnickoIme);
			Trening trening = treningDAO.findOne(treningId);

			Komentar komentar = komentari.get(id);
			if (komentar == null) {
				System.out.println("bilo sta za ocenu");
				komentar = new Komentar(id,tekstKomentara, ocena, datum, korisnik, trening, status, anoniman);
				komentari.put(komentar.getId(), komentar); // dodavanje u kolekciju
			}
		}

		public List<Komentar> getKomentari() {
			return new ArrayList<>(komentari.values());
		}

	}
	
	@Override
	public Komentar findOne(Long id) {
		String sql = 
				"SELECT k.id, k.tekstKomentara, k.ocena, k.datum, k.korisnik, k.treningId, k.status, k.anoniman FROM komentari k " + 
				"LEFT JOIN treninzi tr on tr.id = k.treningId " +
				"WHERE k.id = ? " + 
				"ORDER BY k.id";

		KomentarRowCallBackHandler rowCallbackHandler = new KomentarRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler, id);

		return rowCallbackHandler.getKomentari().get(0);
	}

	@Override
	public List<Komentar> findAll() {
		String sql = 
				"SELECT k.id, k.tekstKomentara, k.ocena, k.datum, k.korisnik, k.treningId, k.status, k.anoniman FROM komentari k " + 
				"LEFT JOIN treninzi tr on tr.id = k.treningId " +
				"ORDER BY k.id";

		KomentarRowCallBackHandler rowCallbackHandler = new KomentarRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler);

		return rowCallbackHandler.getKomentari();
	}
	
	@Transactional
	@Override
	public int save(Komentar komentar) {
		PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				String sql = "INSERT INTO komentari (id, tekstKomentara, ocena, datum, korisnik, treningId, status, anoniman) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

				PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				int index = 1;
				
				preparedStatement.setLong(index++, komentar.getId());
				preparedStatement.setString(index++, komentar.getTekstKomentara());
				preparedStatement.setInt(index++, komentar.getOcena());
				preparedStatement.setString(index++, komentar.getDatum());
				preparedStatement.setString(index++, komentar.getKorisnik().getKorisnickoIme());
				preparedStatement.setLong(index++, komentar.getTrening().getId());
				preparedStatement.setString(index++, komentar.getStatus());
				preparedStatement.setBoolean(index++, komentar.isAnoniman());

				return preparedStatement;
			}

		};
		GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
		boolean uspeh = jdbcTemplate.update(preparedStatementCreator, keyHolder) == 1;
		return uspeh?1:0;
	}
	/*
	@Transactional
	@Override
	public int update(TipTreninga tipTreninga) {
		String sql = "UPDATE tipoviTreninga SET ime = ?, opis = ? WHERE id = ?";	
		boolean uspeh = jdbcTemplate.update(sql, tipTreninga.getIme() , tipTreninga.getOpis()) == 1;
		
		return uspeh?1:0;
	}
	*/
	@Transactional
	@Override
	public int delete(Long id) {
		String sql = "DELETE FROM komentari WHERE id = ?";
		return jdbcTemplate.update(sql, id);
	}

	@Override
	public int update(Komentar komentar) {
		// TODO Auto-generated method stub
		return 0;
	}

}
