package com.golubGym.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.golubGym.dao.TipTreningaDAO;
import com.golubGym.dao.TreningDAO;
import com.golubGym.model.TipTreninga;
import com.golubGym.model.Trening;

@Repository
public class TreningDAOImpl implements TreningDAO{

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private TipTreningaDAO tipTreningaDAO;
	
	private class TreningRowCallBackHandler implements RowCallbackHandler {

		private Map<Long, Trening> treninzi = new LinkedHashMap<>();
		
		@Override
		public void processRow(ResultSet resultSet) throws SQLException {
			System.out.println("processRow");
			int index = 1;
			Long id = resultSet.getLong(index++);
			System.out.println("processRow");
			String naziv = resultSet.getString(index++);
			String trener = resultSet.getString(index++);
			String kratakOpis = resultSet.getString(index++);
			
			System.out.println("processRow");
			Long tipTreningaId = resultSet.getLong(index++);
			System.out.println("processRow" + tipTreningaId);
			String cena = resultSet.getString(index++);
			String vrstaTreninga = resultSet.getString(index++);
			String nivoTreninga = resultSet.getString(index++);
			String trajanjeTreninga = resultSet.getString(index++);
			
			TipTreninga tipTreninga = tipTreningaDAO.findOne(tipTreningaId);

			Trening trening = treninzi.get(id);
			if (trening == null) {
				trening = new Trening(id,naziv, trener, kratakOpis, tipTreninga, cena, vrstaTreninga, nivoTreninga, trajanjeTreninga);
				treninzi.put(trening.getId(), trening); // dodavanje u kolekciju
			}
		}

		public List<Trening> getTreninzi() {
			return new ArrayList<>(treninzi.values());
		}

	}
	
	@Override
	public Trening findOne(Long id) {
		String sql = 
				"SELECT tr.id, tr.naziv, tr.trener, tr.kratakOpis, tr.tipTreningaId, tr.cena, tr.vrstaTreninga, tr.nivoTreninga, tr.trajanjeTreninga FROM treninzi tr " +
				"LEFT JOIN tipovitreninga tp on tp.id = tr.tipTreningaId " +
				"WHERE tr.id = ? " + 
				"ORDER BY tr.id";

		TreningRowCallBackHandler rowCallbackHandler = new TreningRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler, id);

		return rowCallbackHandler.getTreninzi().get(0);
	}
	
	@Override
	public Trening findOne(String naziv) {
		String sql = 
				"SELECT tr.id, tr.naziv, tr.trener, tr.kratakOpis, tr.tipTreningaId, tr.cena, tr.vrstaTreninga, tr.nivoTreninga, tr.trajanjeTreninga FROM treninzi tr " +
						"LEFT JOIN tipovitreninga tp on tp.id = tr.tipTreningaId " + 
						"WHERE tr.naziv = ? " + 
						"ORDER BY tr.naziv";

		TreningRowCallBackHandler rowCallbackHandler = new TreningRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler, naziv);

		return rowCallbackHandler.getTreninzi().get(0);
	}

	@Override
	public List<Trening> findAll() {
		String sql = 
				"SELECT tr.id, tr.naziv, tr.trener, tr.kratakOpis, tp.id, tr.cena, tr.vrstaTreninga, tr.nivoTreninga, tr.trajanjeTreninga FROM treninzi tr " +
						"LEFT JOIN tipovitreninga tp on tp.id = tr.tipTreningaId " +
						"ORDER BY tr.id";

		TreningRowCallBackHandler rowCallbackHandler = new TreningRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler);

		return rowCallbackHandler.getTreninzi();
	}
	
	@Transactional
	@Override
	public int save(Trening trening) {
		PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				String sql = "INSERT INTO treninzi (id, naziv, trener, kratakOpis, tipTreningaId, cena, vrstaTreninga, nivoTreninga, trajanjeTreninga) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

				PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				int index = 1;
				
				preparedStatement.setLong(index++, trening.getId());
				preparedStatement.setString(index++, trening.getNaziv());
				preparedStatement.setString(index++, trening.getTrener());
				preparedStatement.setString(index++, trening.getKratakOpis());
				preparedStatement.setLong(index++, trening.getTipTreninga().getId());
				preparedStatement.setString(index++, trening.getCena());
				preparedStatement.setString(index++, trening.getVrstaTreninga());
				preparedStatement.setString(index++, trening.getNivoTreninga());
				preparedStatement.setString(index++, trening.getTrajanjeTreninga());

				return preparedStatement;
			}

		};
		GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
		boolean uspeh = jdbcTemplate.update(preparedStatementCreator, keyHolder) == 1;
		return uspeh?1:0;
	}
	
	@Transactional
	@Override
	public int update(Trening trening) {
		String sql = "UPDATE treninzi SET naziv = ?, trener = ?, kratakOpis = ?,tipTreningaId = ?, cena = ?, vrstaTreninga = ?, nivoTreninga = ?, trajanjeTreninga = ? WHERE id = ?";	
		boolean uspeh = jdbcTemplate.update(sql, trening.getNaziv(), trening.getTrener(), trening.getKratakOpis(), trening.getTipTreninga().getId(), trening.getCena(), trening.getVrstaTreninga(), trening.getNivoTreninga(), trening.getTrajanjeTreninga()) == 1;
		
		return uspeh?1:0;
	}
	
	@Transactional
	@Override
	public int delete(Long id) {
		String sql = "DELETE FROM treninzi WHERE id = ?";
		return jdbcTemplate.update(sql, id);
	}

}