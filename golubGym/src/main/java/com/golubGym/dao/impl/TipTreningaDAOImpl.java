package com.golubGym.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.golubGym.dao.TipTreningaDAO;
import com.golubGym.model.TipTreninga;

@Repository
public class TipTreningaDAOImpl implements TipTreningaDAO{

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private class TipTreningaRowCallBackHandler implements RowCallbackHandler {

		private Map<Long, TipTreninga> tipoviTreninga = new LinkedHashMap<>();
		
		@Override
		public void processRow(ResultSet resultSet) throws SQLException {
			int index = 1;
			Long id = resultSet.getLong(index++);
			String ime = resultSet.getString(index++);
			String opis = resultSet.getString(index++);

			TipTreninga tipTreninga = tipoviTreninga.get(id);
			if (tipTreninga == null) {
				tipTreninga = new TipTreninga(id,ime, opis);
				tipoviTreninga.put(tipTreninga.getId(), tipTreninga); // dodavanje u kolekciju
			}
		}

		public List<TipTreninga> getTipoviTreninga() {
			return new ArrayList<>(tipoviTreninga.values());
		}

	}
	
	@Override
	public TipTreninga findOne(String ime) {
		
		System.out.println(ime + "ime tipa treninga");
		
		String sql = 
				"SELECT tip.id, tip.ime, tip.opis FROM tipovitreninga tip " + 
				"WHERE tip.id = ? " + 
				"ORDER BY tip.id";

		TipTreningaRowCallBackHandler rowCallbackHandler = new TipTreningaRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler, ime);
		System.out.println(rowCallbackHandler.getTipoviTreninga().size() + "nesaedfsdfasa");
		
		return rowCallbackHandler.getTipoviTreninga().get(0);
	}
	
	@Override
	public TipTreninga findOne(Long id) {
		String sql = 
				"SELECT tip.id, tip.ime, tip.opis FROM tipovitreninga tip " + 
				"WHERE tip.id = ? " + 
				"ORDER BY tip.id";

		TipTreningaRowCallBackHandler rowCallbackHandler = new TipTreningaRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler, id);

		return rowCallbackHandler.getTipoviTreninga().get(0);
	}
	
	@Override
	public TipTreninga findOne(String ime, String opis) {
		String sql = 
				"SELECT tip.id, tip.ime, tip.opis FROM tipovitreninga tip " + 
				"WHERE tip.ime = ? AND " +
				"tip.opis = ? " + 
				"ORDER BY tip.id";

		TipTreningaRowCallBackHandler rowCallbackHandler = new TipTreningaRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler, ime, opis);

		if(rowCallbackHandler.getTipoviTreninga().size() == 0) {
			return null;
		}
		
		return rowCallbackHandler.getTipoviTreninga().get(0);
	}

	@Override
	public List<TipTreninga> findAll() {
		String sql = 
				"SELECT tip.id, tip.ime, tip.opis FROM tipovitreninga tip " + 
				"ORDER BY tip.id";

		TipTreningaRowCallBackHandler rowCallbackHandler = new TipTreningaRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler);

		return rowCallbackHandler.getTipoviTreninga();
	}
	
	@Transactional
	@Override
	public int save(TipTreninga tipTreninga) {
		PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				String sql = "INSERT INTO tipovitreninga (id, ime, opis) VALUES (?, ?, ?)";

				PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				int index = 1;
				
				preparedStatement.setLong(index++, tipTreninga.getId());
				preparedStatement.setString(index++, tipTreninga.getIme());
				preparedStatement.setString(index++, tipTreninga.getOpis());
			

				return preparedStatement;
			}

		};
		GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
		boolean uspeh = jdbcTemplate.update(preparedStatementCreator, keyHolder) == 1;
		return uspeh?1:0;
	}
	
	@Transactional
	@Override
	public int update(TipTreninga tipTreninga) {
		String sql = "UPDATE tipovitreninga SET ime = ?, opis = ? WHERE id = ?";	
		boolean uspeh = jdbcTemplate.update(sql, tipTreninga.getIme() , tipTreninga.getOpis()) == 1;
		
		return uspeh?1:0;
	}
	
	@Transactional
	@Override
	public int delete(Long id) {
		String sql = "DELETE FROM tipovitreninga WHERE id = ?";
		return jdbcTemplate.update(sql, id);
	}

}