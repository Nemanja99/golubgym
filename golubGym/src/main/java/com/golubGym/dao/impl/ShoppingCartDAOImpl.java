package com.golubGym.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.golubGym.dao.KorisnikDAO;
import com.golubGym.dao.ShoppingCartDAO;
import com.golubGym.dao.TipTreningaDAO;
import com.golubGym.model.Korisnik;
import com.golubGym.model.ShoppingCart;
import com.golubGym.model.TipTreninga;

@Repository
public class ShoppingCartDAOImpl implements ShoppingCartDAO{

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private TipTreningaDAO tipTreningaDAO;
	
	@Autowired
	private KorisnikDAO korisnikDAO;
	
	
	private class ShoppingCartRowMapper implements RowMapper<ShoppingCart> {

		@Override
		public ShoppingCart mapRow(ResultSet rs, int rowNum) throws SQLException {
			
			int index = 1;
			
			Long id = rs.getLong(index++);
			String nazivTreninga = rs.getString(index++);
			String trener = rs.getString(index++);
			
			Long tipTreningaId = rs.getLong(index++);
			
			String datumVreme = rs.getString(index++);
			String cena = rs.getString(index++);
			String korisnickoIme = rs.getString(index++);
			
			Korisnik korisnik = korisnikDAO.findOne(korisnickoIme);
			TipTreninga tipTreninga = tipTreningaDAO.findOne(tipTreningaId);
			
			ShoppingCart shoppingCart = new ShoppingCart(id, nazivTreninga, trener, tipTreninga, datumVreme, cena, korisnik);
			return shoppingCart;
			
		}

	}
	
	@Override
	public ShoppingCart findOne(Long id) {
		String sql = 
				"SELECT c.id, c.nazivTreninga, c.trener, c.tipTreningaId, c.datumVreme, c.cena, c.korisnik  FROM shoppingcarts c " 
						+"LEFT JOIN tipovitreninga t ON t.id = c.tipTreningaId "
						+"LEFT JOIN korisnici k ON k.korisnickoIme = c.korisnik "
						+"WHERE c.id = ? " 
						+" ORDER BY c.id";
		
		return jdbcTemplate.queryForObject(sql, new ShoppingCartRowMapper(), id);
		
		
	}
	
	
	@Override
	public List<ShoppingCart> findOne( String korisnickoIme, Long tipTreningaId) {
		String sql = 
				"SELECT c.id,c.nazivTreninga, c.trener, t.id, c.datumVreme, c.cena, k.korisnickoIme FROM shoppingcarts c " 
						+"LEFT JOIN tipoviTreninga t ON t.id = c.tipTreningaId "
						+"LEFT JOIN korisnici k ON k.korisnickoIme = c.korisnik "
						+"WHERE c.korisnik = ? and c.tipTreningaId = ? " 
						+" ORDER BY c.id";
				
		return jdbcTemplate.query(sql, new ShoppingCartRowMapper(), korisnickoIme, tipTreningaId);
	}
	
	@Override
	public List<ShoppingCart> findAll() {
		String sql = 
				"SELECT c.id,c.nazivTreninga, c.trener, t.id, c.datumVreme, c.cena, k.korisnickoIme FROM shoppingcarts c " 
						+"LEFT JOIN tipoviTreninga t ON t.id = c.tipTreningaId "
						+"LEFT JOIN korisnici k ON k.korisnickoIme = c.korisnik "
						+" ORDER BY c.id";
		return jdbcTemplate.query(sql, new ShoppingCartRowMapper());
	}
	
	@Override
	public List<ShoppingCart> findAll(String korisnickoIme) {
		String sql = 
				"SELECT c.id,c.nazivTreninga, c.trener, t.id, c.datumVreme, c.cena, k.korisnickoIme FROM shoppingcarts c " 
				+"LEFT JOIN tipoviTreninga t ON t.id = c.tipTreningaId "
				+"LEFT JOIN korisnici k ON k.korisnickoIme = c.korisnik "
				+"WHERE c.korisnik = ?  AND administrator != true   " 
				+" ORDER BY c.id";
		return jdbcTemplate.query(sql, new ShoppingCartRowMapper(), korisnickoIme);
	}
	
	
	
	@Override
	public List<ShoppingCart> findAll2(String tipTreningaId) {
		String sql = 
				"SELECT c.id,c.nazivTreninga, c.trener, t.id, c.datumVreme, c.cena, k.korisnickoIme FROM shoppingcarts c " 
						+"LEFT JOIN tipoviTreninga t ON t.id = c.tipTreningaId "
						+"LEFT JOIN korisnici k ON k.korisnickoIme = c.korisnik "
						+"WHERE c.tipTreningaId != ? " 
						+" ORDER BY c.id";
				
	
		
		return jdbcTemplate.query(sql, new ShoppingCartRowMapper(), tipTreningaId);
		
	
	}
	
	
	@Override
	public int save(ShoppingCart shoppingCart) {
		
		//INSERT INTO carts (bookId, user) VALUES (1, 'b');
		
		String sql = "INSERT INTO shoppingcarts (id, nazivTreninga, trener, tipTreningaId, datumVreme, cena, korisnik) VALUES (?, ?, ?, ?, ?, ?, ?) ";
		return jdbcTemplate.update(sql,shoppingCart.getId(), shoppingCart.getNazivTreninga() , shoppingCart.getTrener() ,shoppingCart.getTipTreninga().getId(), shoppingCart.getDatumVreme() , shoppingCart.getCena() , shoppingCart.getKorisnik().getKorisnickoIme());
	}
	
	
	@Override
	public int delete(Long id) {
		String sql = "DELETE FROM shoppingcarts WHERE id = ?";
		return jdbcTemplate.update(sql, id);
	}


	@Override
	public int update(ShoppingCart shoppingCart) {
		// TODO Auto-generated method stub
		return 0;
	}
}
