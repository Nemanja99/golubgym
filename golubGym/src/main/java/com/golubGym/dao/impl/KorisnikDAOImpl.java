package com.golubGym.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.golubGym.dao.KorisnikDAO;
import com.golubGym.model.Korisnik;

@Repository
public class KorisnikDAOImpl implements KorisnikDAO{

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private class KorisnikRowCallBackHandler implements RowCallbackHandler {

		private Map<String, Korisnik> korisnici = new LinkedHashMap<>();
		
		@Override
		public void processRow(ResultSet resultSet) throws SQLException {
			int index = 1;
			//Long id = resultSet.getLong(index++);
			String korisnickoIme = resultSet.getString(index++);
			//String lozinka = resultSet.getString(index++);
			String email = resultSet.getString(index++);
			String ime = resultSet.getString(index++);
			String prezime = resultSet.getString(index++);
			String datumRodjenja = resultSet.getString(index++);
			String adresa = resultSet.getString(index++);
			String brojTelefona = resultSet.getString(index++);
			Boolean administrator = resultSet.getBoolean(index++);
			
			//Korisnik korisnik = korisnici.get(id);
			//if (korisnik == null) {
				Korisnik korisnik= new Korisnik(korisnickoIme, null, email, ime, prezime, datumRodjenja, adresa, brojTelefona, administrator);
				korisnici.put(korisnik.getKorisnickoIme(), korisnik); // dodavanje u kolekciju
			//}
		}

		public List<Korisnik> getKorisnici() {
			return new ArrayList<>(korisnici.values());
		}

	}
	
	/*@Override
	public Korisnik findOne(Long id) {
		String sql = 
				"SELECT kor.id, kor.korisnickoIme,kor.email, kor.ime, kor.prezime, kor.datumRodjenja, kor.adresa, kor.brojTelefona, kor.administrator FROM korisnici kor " + 
				"WHERE kor.id = ? " + 
				"ORDER BY kor.id";

		KorisnikRowCallBackHandler rowCallbackHandler = new KorisnikRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler, id);

		return rowCallbackHandler.getKorisnici().get(0);
	}*/
	
	@Override
	public Korisnik findOne(String korisnickoIme) {
		String sql = 
				"SELECT kor.korisnickoIme,kor.email, kor.ime, kor.prezime, kor.datumRodjenja, kor.adresa, kor.brojTelefona, kor.administrator FROM korisnici kor " + 
				"WHERE kor.korisnickoIme = ? " + 
				"ORDER BY kor.korisnickoIme";

		KorisnikRowCallBackHandler rowCallbackHandler = new KorisnikRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler, korisnickoIme);

		return rowCallbackHandler.getKorisnici().get(0);
	}

	@Override
	public Korisnik findOne(Boolean administrator) {
		String sql = 
				"SELECT kor.korisnickoIme,kor.email, kor.ime, kor.prezime, kor.datumRodjenja, kor.adresa, kor.brojTelefona, kor.administrator FROM korisnici kor " + 
				"WHERE kor.administrator = ? " + 
				"ORDER BY kor.administrator";

		KorisnikRowCallBackHandler rowCallbackHandler = new KorisnikRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler, administrator);

		return rowCallbackHandler.getKorisnici().get(0);
	}
	
	@Override
	public Korisnik findOne(String korisnickoIme, String lozinka) {
		String sql = 
				"SELECT kor.korisnickoIme, kor.email, kor.ime, kor.prezime, kor.datumRodjenja, kor.adresa, kor.brojTelefona, kor.administrator  FROM korisnici kor " + 
				"WHERE kor.korisnickoIme = ? AND " +
				"kor.lozinka = ? " + 
				"ORDER BY kor.ime";

		KorisnikRowCallBackHandler rowCallbackHandler = new KorisnikRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler, korisnickoIme, lozinka);

		if(rowCallbackHandler.getKorisnici().size() == 0) {
			return null;
		}
		
		return rowCallbackHandler.getKorisnici().get(0);
	}

	@Override
	public List<Korisnik> findAll() {
		String sql = 
				"SELECT korisnickoIme, email, ime, prezime, datumRodjenja, adresa, brojTelefona, administrator  FROM korisnici " + 
				"ORDER BY ime";

		KorisnikRowCallBackHandler rowCallbackHandler = new KorisnikRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler);
		System.out.println(rowCallbackHandler.getKorisnici().size());
		return rowCallbackHandler.getKorisnici();
	}
	
	@Transactional
	@Override
	public int save(Korisnik korisnik) {
		PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				String sql = "INSERT INTO korisnici (korisnickoIme, lozinka, email, ime, prezime, datumRodjenja, adresa, brojTelefona ,administrator) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

				PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				int index = 1;
				
				preparedStatement.setString(index++, korisnik.getKorisnickoIme());
				preparedStatement.setString(index++, korisnik.getLozinka());
				preparedStatement.setString(index++, korisnik.getEmail());
				preparedStatement.setString(index++, korisnik.getIme());
				preparedStatement.setString(index++, korisnik.getPrezime());
				preparedStatement.setString(index++, korisnik.getDatumRodjenja());
				preparedStatement.setString(index++, korisnik.getAdresa());
				preparedStatement.setString(index++, korisnik.getBrojTelefona());
				preparedStatement.setBoolean(index++, korisnik.isAdministrator());

				return preparedStatement;
			}

		};
		GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
		boolean uspeh = jdbcTemplate.update(preparedStatementCreator, keyHolder) == 1;
		return uspeh?1:0;
	}
	
	@Transactional
	@Override
	public int update(Korisnik korisnik) {
		String sql = "UPDATE korisnici SET korisnickoIme = ?, lozinka = ?, email = ?,ime = ?, prezime = ?, datumRodjenja = ?, adresa = ?, brojTelefona = ?, administrator = ? WHERE korisnickoIme = ?";	
		boolean uspeh = jdbcTemplate.update(sql, korisnik.getKorisnickoIme() , korisnik.getLozinka(), korisnik.getEmail(), korisnik.getIme() , korisnik.getPrezime(), korisnik.getDatumRodjenja(), korisnik.getAdresa(), korisnik.getBrojTelefona(), korisnik.isAdministrator()) == 1;
		
		return uspeh?1:0;
	}
	
	@Transactional
	@Override
	public int delete(String korisnickoIme) {
		String sql = "DELETE FROM korisnici WHERE korisnickoIme = ?";
		return jdbcTemplate.update(sql, korisnickoIme);
	}

}
