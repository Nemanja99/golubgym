package com.golubGym.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.golubGym.dao.ClanskaKarticaDAO;
import com.golubGym.model.ClanskaKartica;

@Repository
public class ClanskaKarticaDAOImpl implements ClanskaKarticaDAO{
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private class ClanskaKarticaRowCallBackHandler implements RowCallbackHandler {

		private Map<Long, ClanskaKartica> clanskeKartice = new LinkedHashMap<>();
		
		@Override
		public void processRow(ResultSet resultSet) throws SQLException {
			int index = 1;
			Long id = resultSet.getLong(index++);
			String popust = resultSet.getString(index++);
			Integer brojPoena = resultSet.getInt(index++);
			

			ClanskaKartica clanskaKartica = clanskeKartice.get(id);
			if (clanskaKartica == null) {
				clanskaKartica = new ClanskaKartica(id, popust, brojPoena);
				clanskeKartice.put(clanskaKartica.getId(), clanskaKartica); // dodavanje u kolekciju
			}
		}

		public List<ClanskaKartica> getClanskeKartice() {
			return new ArrayList<>(clanskeKartice.values());
		}

	}
	
	@Override
	public ClanskaKartica findOne(Long id) {
		String sql = 
				"SELECT id, popust, brojPoena FROM clanskakartica " + 
				"WHERE id = ? " + 
				"ORDER BY id";

		ClanskaKarticaRowCallBackHandler rowCallbackHandler = new ClanskaKarticaRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler, id);

		return rowCallbackHandler.getClanskeKartice().get(0);
	}

	@Override
	public List<ClanskaKartica> findAll() {
		String sql = 
				"SELECT id, popust, brojPoena FROM clanskakartica " + 
				"ORDER BY id";

		ClanskaKarticaRowCallBackHandler rowCallbackHandler = new ClanskaKarticaRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler);

		return rowCallbackHandler.getClanskeKartice();
	}
	
	@Transactional
	@Override
	public int save(ClanskaKartica clanskaKartica) {
		PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				String sql = "INSERT INTO clanskakartica (id, popust, brojPoena) VALUES (?, ?, ?)";

				PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				int index = 1;
				
				preparedStatement.setLong(index++, clanskaKartica.getId());
				preparedStatement.setString(index++, clanskaKartica.getPopust());
				preparedStatement.setInt(index++, clanskaKartica.getBrojPoena());

				return preparedStatement;
			}

		};
		GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
		boolean uspeh = jdbcTemplate.update(preparedStatementCreator, keyHolder) == 1;
		return uspeh?1:0;
	}
	
	@Transactional
	@Override
	public int update(ClanskaKartica clanskaKartica) {
		String sql = "UPDATE clanskakartica SET popust = ?, brojPoena = ? WHERE id = ?";	
		return jdbcTemplate.update(sql, clanskaKartica.getPopust() , clanskaKartica.getBrojPoena() , clanskaKartica.getId());
		
	}
	
	@Transactional
	@Override
	public int delete(Long id) {
		String sql = "DELETE FROM clanskaKartica WHERE id = ?";
		return jdbcTemplate.update(sql, id);
	}
}