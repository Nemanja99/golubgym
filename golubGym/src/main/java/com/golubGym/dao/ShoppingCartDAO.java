package com.golubGym.dao;

import java.util.List;

import com.golubGym.model.ShoppingCart;

public interface ShoppingCartDAO {

	public List<ShoppingCart> findAll();
	
	public ShoppingCart findOne(Long id);
	
	public List<ShoppingCart> findOne(String korisnickoIme, Long tipTreningaId);
	
	public List<ShoppingCart> findAll(String korisnickoIme);
	
	public List<ShoppingCart> findAll2(String tipTreningaId);

	public int save(ShoppingCart shoppingCart);

	public int update(ShoppingCart shoppingCart);

	public int delete(Long id);
	
}
