package com.golubGym.dao;

import java.util.List;

import com.golubGym.model.Korisnik;

public interface KorisnikDAO {

//public Korisnik findOne(Long id);
	
	public Korisnik findOne(String korisnickoIme); 
	
	public Korisnik findOne(Boolean administrator); 
	
	public Korisnik findOne(String korisnickoIme, String lozinka);

	public List<Korisnik> findAll();

	public int save(Korisnik korisnik);

	public int update(Korisnik korisnik);

	public int delete(String korisnickoIme);
	
}
