package com.golubGym.dao;

import java.util.List;

import com.golubGym.model.TipTreninga;

public interface TipTreningaDAO {

	public TipTreninga findOne(String ime);
	
	public TipTreninga findOne(Long id);
	
	public TipTreninga findOne(String ime, String opis);

	public List<TipTreninga> findAll();

	public int save(TipTreninga tipTreninga);

	public int update(TipTreninga tipTreninga);

	public int delete(Long id);
	
}
