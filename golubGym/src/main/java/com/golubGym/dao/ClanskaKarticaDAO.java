package com.golubGym.dao;

import java.util.List;

import com.golubGym.model.ClanskaKartica;

public interface ClanskaKarticaDAO {

	public ClanskaKartica findOne(Long id);

	public List<ClanskaKartica> findAll();

	public int save(ClanskaKartica clanskaKartica);

	public int update(ClanskaKartica clanskaKartica);

	public int delete(Long id);
	
}
